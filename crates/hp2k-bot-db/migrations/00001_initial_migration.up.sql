CREATE TABLE doctors (
  code VARCHAR(40) NOT NULL,
  name VARCHAR(80) NOT NULL,
  speciality VARCHAR(40) NOT NULL,
  speciality_code VARCHAR(40) NOT NULL,

  user_id BIGINT UNSIGNED,
  chat_id BIGINT,

  CONSTRAINT pk__doctors PRIMARY KEY (code),
  CONSTRAINT uq__doctors__chat_id UNIQUE (chat_id),
  CONSTRAINT uq__doctors__user_id UNIQUE (user_id)
);

CREATE TABLE otws (
  id INT UNSIGNED NOT NULL  AUTO_INCREMENT,
  doctor_code VARCHAR(40) NOT NULL,
  otw_date DATE NOT NULL,
  otw_at DATETIME NOT NULL,
  eta_at DATETIME NOT NULL,
  arrive_at DATETIME,

  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  CONSTRAINT pk__otws PRIMARY KEY (id),
  CONSTRAINT uq__otws__doctor_code__otw_date UNIQUE (doctor_code,otw_date),
  CONSTRAINT fk__otws__doctors FOREIGN KEY (doctor_code) REFERENCES doctors (code) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE schedules (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  doctor_code VARCHAR(40) NOT NULL,
  schedule_date DATE NOT NULL,
  schedule_at DATETIME NOT NULL,

  CONSTRAINT pk__schedules PRIMARY KEY (id),
  CONSTRAINT fk__schedules__doctor FOREIGN KEY (doctor_code) REFERENCES doctors (code) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT uq__schedules__doctor_code__schedule_date UNIQUE (doctor_code,schedule_date)
);

CREATE TABLE tasks (
  id BINARY(16) NOT NULL,
  task_name VARCHAR(80) NOT NULL,
  payload TEXT NOT NULL,

  execute_at DATETIME NOT NULL,
  executed_at DATETIME,

  CONSTRAINT pk__tasks PRIMARY KEY (id)
);
