use anyhow::{anyhow, Context};
use chrono::{DateTime, Local, Utc};
use hp2k_bot_core::{Task, TaskStorageError, TaskStorageResult, TaskStorageService};
use sqlx::prelude::*;
use uuid::Uuid;

use crate::MySqlSqlxDatabase;

#[derive(Debug, FromRow)]
struct MyTask {
    id: Uuid,
    task_name: String,
    payload: String,
    execute_at: DateTime<Utc>,
    executed_at: Option<DateTime<Utc>>,
}

impl From<MyTask> for Task {
    fn from(v: MyTask) -> Self {
        Self {
            id: v.id,
            name: v.task_name,
            execute_at: v.execute_at,
            executed_at: v.executed_at,
            payload: v.payload,
        }
    }
}

#[async_trait::async_trait]
impl TaskStorageService for MySqlSqlxDatabase {
    async fn get_tasks(&self, execute_at: DateTime<Local>) -> TaskStorageResult<Vec<Task>> {
        let query =
            "SELECT id,task_name,payload,execute_at,executed_at FROM tasks WHERE execute_at <= ? AND executed_at IS NULL";

        let tasks = sqlx::query_as::<_, MyTask>(query)
            .bind(execute_at)
            .fetch_all(&self.pool)
            .await
            .map_err(TaskStorageError::get_tasks_failed)?
            .into_iter()
            .map(From::from)
            .collect();

        Ok(tasks)
    }

    async fn mark_task_executed(&self, task_id: Uuid) -> TaskStorageResult<()> {
        let now = chrono::Local::now();
        let query = "UPDATE tasks SET executed_at = ? WHERE id = ?";

        let affected = sqlx::query(query)
            .bind(now)
            .bind(task_id)
            .execute(&self.pool)
            .await
            .context("error when executing")?
            .rows_affected();

        if affected == 0 {
            return Err(TaskStorageError::TaskNotFound(String::from(task_id)));
        }
        Ok(())
    }

    async fn create_task(
        &self,
        task_name: &str,
        payload: &str,
        execute_at: DateTime<Local>,
    ) -> TaskStorageResult<Task> {
        let query = "insert into tasks (id,task_name,payload,execute_at) values (?,?,?,?)";
        let id = uuid::Uuid::new_v4();

        sqlx::query(query)
            .bind(id)
            .bind(task_name)
            .bind(payload)
            .bind(execute_at)
            .execute(&self.pool)
            .await
            .context("error when creating tasks")?;

        let select_query =
            "select id,task_name,payload,execute_at,executed_at from tasks where id = ?";
        let result = sqlx::query_as::<_, MyTask>(select_query)
            .bind(id)
            .fetch_optional(&self.pool)
            .await
            .map_err(TaskStorageError::create_task_failed)?
            .ok_or(TaskStorageError::TaskNotFound(String::from(id)))?;
        Ok(result.into())
    }

    async fn cancel_task(&self, id: &str) -> TaskStorageResult<()> {
        let query = "delete from tasks where id = ?";
        let result = sqlx::query(query)
            .bind(id)
            .execute(&self.pool)
            .await
            .map_err(TaskStorageError::delete_task_failed)?;

        if result.rows_affected() == 0 {
            return Err(anyhow!("task not found").into());
        }
        Ok(())
    }
}
