use anyhow::Context;
use hp2k_bot_core::{DoctorCreate, StorageServiceResult};
use sqlx::{MySqlPool, QueryBuilder};
use tracing::{debug, info};

use crate::types::DoctorCode;

pub async fn compare_doctors(
    pool: &MySqlPool,
    doctors: &[DoctorCreate],
) -> StorageServiceResult<(Vec<String>, Vec<String>)> {
    let new_codes: Vec<_> = doctors.iter().map(|doc| &doc.code).collect();
    let mut qb = QueryBuilder::new("WITH my_id(id) AS (VALUES ");

    let mut sp = qb.separated(",");
    for code in new_codes {
        sp.push(format!("('{code}')"));
    }
    qb.push(
        r#")
            SELECT m.id as new_id, d.code as old_id
            FROM my_id m
            RIGHT JOIN doctors d
            ON d.code = m.id
            "#,
    );
    let codes = qb
        .build_query_as::<DoctorCode>()
        .fetch_all(pool)
        .await
        .context("failed when get doctors code")?;

    let mut update = Vec::new();
    let mut delete = Vec::new();
    for code in codes {
        match code {
            DoctorCode {
                old_id: Some(id),
                new_id: None,
            } => {
                debug!("{} delete", id);
                delete.push(id);
            }
            DoctorCode {
                old_id: Some(id_old),
                new_id: Some(id_new),
            } if id_old == id_new => {
                debug!("{} update", id_old);
                update.push(id_old)
            }
            _ => {}
        };
    }

    Ok((update, delete))
}

pub async fn create_doctor<'c>(
    pool: &mut sqlx::Transaction<'c, sqlx::MySql>,
    doctors: &[DoctorCreate],
) -> StorageServiceResult<()> {
    if doctors.is_empty() {
        return Ok(());
    }

    for doctor_chunk in doctors.chunks(10) {
        let mut create_doctor_q = QueryBuilder::new(
            r#"
        INSERT INTO doctors (
            code,
            name,
            speciality,
            speciality_code
        ) "#,
        );
        create_doctor_q.push_values(doctor_chunk, |mut bd, tup| {
            info!(
                "creating doctor {} : {} - {} - {}",
                tup.code, tup.name, tup.speciality_code, tup.speciality_code
            );
            bd.push_bind(&tup.code)
                .push_bind(&tup.name)
                .push_bind(&tup.speciality)
                .push_bind(&tup.speciality_code);
        });

        create_doctor_q
            .build()
            .execute(&mut **pool)
            .await
            .context("Error when creating doktor")?;
    }

    Ok(())
}

pub async fn update_doctor<'c>(
    pool: &mut sqlx::Transaction<'c, sqlx::MySql>,
    doctors: &[DoctorCreate],
) -> StorageServiceResult<()> {
    if doctors.is_empty() {
        return Ok(());
    }
    for doctor in doctors {
        info!(
            "updating doctor {} : {} - {} - {}",
            doctor.code, doctor.name, doctor.speciality_code, doctor.speciality_code
        );

        sqlx::query(
            r#"
        UPDATE doctors
        SET 
            name = ?,
            speciality = ?,
            speciality_code = ?
        WHERE 
            code = ?
    "#,
        )
        .bind(&doctor.name)
        .bind(&doctor.speciality)
        .bind(&doctor.speciality_code)
        .bind(&doctor.code)
        .execute(&mut **pool)
        .await
        .context("Error when updating doctors")?;
    }
    Ok(())
}

pub async fn delete_doctor<'c>(
    pool: &mut sqlx::Transaction<'c, sqlx::MySql>,
    doctors: &[String],
) -> StorageServiceResult<()> {
    if doctors.is_empty() {
        return Ok(());
    }

    let mut qb = QueryBuilder::new("DELETE FROM doctors WHERE code IN (");

    let mut sp = qb.separated(",");
    for doctor in doctors {
        sp.push_bind(doctor);
    }

    sp.push_unseparated(")");

    doctors.iter().for_each(|i| info!("deleting doctor {}", i));

    qb.build()
        .execute(&mut **pool)
        .await
        .context("Error when deleting doctors")?;

    Ok(())
}
