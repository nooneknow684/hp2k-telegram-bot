pub mod sync_doctors {
    use chrono::{Duration, Local, NaiveDate, NaiveDateTime, Timelike};
    use hp2k_bot_core::StorageService;
    use sqlx::{
        mysql::{MySqlConnectOptions, MySqlPoolOptions},
        Row,
    };

    use crate::MySqlSqlxDatabase;
    use fake::{faker, Fake};

    #[sqlx::test]
    async fn test_doctor_will_otw(
        opt: MySqlPoolOptions,
        conn: MySqlConnectOptions,
    ) -> anyhow::Result<()> {
        let service = MySqlSqlxDatabase::create(opt, conn);
        let query = "INSERT INTO doctors (code,name,speciality,speciality_code) VALUES (?,?,?,?)";

        let mut codes = Vec::new();

        for _ in 0..10 {
            let code = faker::number::en::NumberWithFormat("D######").fake::<String>();
            codes.push(String::from(&code));

            sqlx::query(query)
                .bind(&code)
                .bind(faker::name::en::NameWithTitle().fake::<String>())
                .bind("-")
                .bind("-")
                .execute(service.get_pool())
                .await?;
        }

        let base_date = NaiveDate::from_ymd_opt(2023, 12, 12)
            .unwrap()
            .and_hms_opt(12, 5, 9)
            .unwrap()
            .and_local_timezone(Local)
            .latest()
            .unwrap();

        let otw_at = base_date.with_minute(1).unwrap();
        let eta_at = otw_at.checked_add_signed(Duration::hours(10)).unwrap();

        service
            .assign_otw(
                codes.get(0).unwrap(),
                otw_at.date_naive(),
                otw_at.into(),
                eta_at.into(),
            )
            .await?;

        println!(
            "{:?}",
            sqlx::query("select * from otws")
                .fetch_all(service.get_pool())
                .await?
                .into_iter()
                .map(|e| e.try_get("updated_at").unwrap())
                .collect::<Vec<NaiveDateTime>>()
        );

        let doctors = service.get_doctors_will_otw(base_date, 5).await?;

        assert_eq!(doctors.len(), 1);
        assert_eq!(
            doctors.get(0).map(|item| &item.code),
            Some(codes.get(0).unwrap())
        );

        Ok(())
    }
}
