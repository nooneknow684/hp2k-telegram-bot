use crate::{
    helper::{compare_doctors, create_doctor, delete_doctor, update_doctor},
    types::{Doctor, DoctorOtw, Otw, Schedule},
    MySqlSqlxDatabase,
};
use anyhow::{anyhow, Context};
use chrono::{DateTime, Duration, NaiveDate, Timelike, Utc};
use hp2k_bot_core::{
    CountedList, Doctor as ServiceDoctor, DoctorCreate, Migratable, Otw as ServiceOtw,
    StorageService, StorageServiceError, StorageServiceResult,
};
use sqlx::{QueryBuilder, Row};
use tracing::instrument;

#[async_trait::async_trait]
impl Migratable for MySqlSqlxDatabase {
    async fn migrate(&self) -> anyhow::Result<()> {
        println!("migrating msqlx");
        Ok(sqlx::migrate!("./migrations").run(&self.pool).await?)
    }
}

#[async_trait::async_trait]
impl StorageService for MySqlSqlxDatabase {
    #[instrument(skip_all)]
    async fn sync_doctors(&self, doctors: Vec<DoctorCreate>) -> StorageServiceResult<()> {
        let (update_code, delete_code) = compare_doctors(&self.pool, &doctors).await?;

        let (update_doctors, create_doctors): (Vec<_>, Vec<_>) = doctors
            .into_iter()
            .partition(|doctor| update_code.contains(&doctor.code));

        let mut transaction = self
            .pool
            .begin()
            .await
            .context("error when creating transaction")?;

        create_doctor(&mut transaction, &create_doctors).await?;
        update_doctor(&mut transaction, &update_doctors).await?;
        delete_doctor(&mut transaction, &delete_code).await?;

        transaction
            .commit()
            .await
            .context("error when running transaction")?;

        Ok(())
    }

    async fn get_doctor(&self, doctor_code: &str) -> StorageServiceResult<ServiceDoctor> {
        let doctor = sqlx::query_as::<_, Doctor>(
            r#"SELECT 
                code,
                name,
                speciality,
                speciality_code,
                user_id,
                chat_id
            FROM doctors 
            WHERE code = ?"#,
        )
        .bind(doctor_code)
        .fetch_optional(&self.pool)
        .await
        .context(format!(
            "Error when getting doctors with code : {doctor_code}"
        ))?
        .map(Into::into)
        .ok_or(StorageServiceError::DoctorCodeNotFound {
            doctor_code: String::from(doctor_code),
        })?;
        Ok(doctor)
    }

    async fn get_doctors(
        &self,
        doctor_name: &str,
        page: i64,
    ) -> StorageServiceResult<CountedList<ServiceDoctor>> {
        let mut count_qb = QueryBuilder::new("SELECT COUNT(*) as count FROM doctors WHERE 1 = 1");
        let mut qb = QueryBuilder::new(
            r#"SELECT
                code,
                name,
                speciality,
                speciality_code,
                user_id,
                chat_id
            FROM doctors
            WHERE 1 = 1
            "#,
        );

        if !doctor_name.is_empty() {
            qb.push(" AND name LIKE ");
            qb.push_bind(format!("%{doctor_name}%"));

            count_qb.push(" AND name LIKE ");
            count_qb.push_bind(format!("%{doctor_name}%"));
        };

        qb.push(" LIMIT ")
            .push_bind(10)
            .push(" OFFSET ")
            .push_bind((page - 1) * 10);

        let doctors_count = count_qb
            .build()
            .fetch_one(&self.pool)
            .await
            .context("error when fetching doctors count")?
            .try_get("count")
            .context("error when getting row count")?;
        let doctors = qb
            .build_query_as::<Doctor>()
            .fetch_all(&self.pool)
            .await
            .context("Error when getting doctors")?;

        Ok(CountedList {
            data: doctors.into_iter().map(Into::into).collect(),
            count: doctors_count,
        })
    }

    async fn get_doctor_by_user_id(&self, user_id: u64) -> StorageServiceResult<ServiceDoctor> {
        let doctor = sqlx::query_as::<_,Doctor>("SELECT code,name,speciality,speciality_code,user_id,chat_id FROM doctors where user_id = ?").bind(user_id).fetch_optional(&self.pool).await 
            .context(format!("error when getting doctor with user_id : {user_id}"))?
            .map(Into::into)
            .ok_or(StorageServiceError::DoctorUserNotFound { user_id })?;
        Ok(doctor)
    }

    async fn get_doctor_by_chat_id(&self, chat_id: i64) -> StorageServiceResult<ServiceDoctor> {
        let doctor = sqlx::query_as::<_,Doctor>("SELECT code,name,speciality,speciality_code,user_id,chat_id FROM doctors where user_id = ?").bind(chat_id).fetch_optional(&self.pool).await 
            .context(format!("error when getting doctor with user_id : {chat_id}"))?
            .map(Into::into)
            .ok_or(StorageServiceError::DoctorChatNotFound { chat_id })?;
        Ok(doctor)
    }

    async fn get_user(&self, _user_id: u32) -> StorageServiceResult<bool> {
        todo!()
    }

    async fn map_doctor(
        &self,
        doctor_code: &str,
        user_id: u64,
        chat_id: i64,
    ) -> StorageServiceResult<()> {
        let rows_affected =
            sqlx::query("UPDATE doctors set user_id = ?, chat_id = ? WHERE code = ?")
                .bind(user_id)
                .bind(chat_id)
                .bind(doctor_code)
                .execute(&self.pool)
                .await
                .context(format!(
                    "error when mapping doctors with code {doctor_code}"
                ))?
                .rows_affected();

        if rows_affected == 0 {
            return Err(StorageServiceError::DoctorCodeNotFound {
                doctor_code: String::from(doctor_code),
            });
        }

        Ok(())
    }

    async fn unmap_user(&self, user_id: u64, chat_id: i64) -> StorageServiceResult<()> {
        let rows_affected = sqlx::query(
            "UPDATE doctors set user_id = null, chat_id = null WHERE user_id = ? AND chat_id = ?",
        )
        .bind(user_id)
        .bind(chat_id)
        .execute(&self.pool)
        .await
        .context(format!(
            "error when unmapping user with user_id = {user_id} and chat_id = {chat_id}"
        ))?
        .rows_affected();

        if rows_affected == 0 {
            return Err(StorageServiceError::UserNotFound { chat_id, user_id });
        }

        Ok(())
    }

    async fn assign_otw(
        &self,
        doctor_code: &str,
        otw_date: NaiveDate,
        otw_at: DateTime<Utc>,
        eta_at: DateTime<Utc>,
    ) -> StorageServiceResult<ServiceOtw> {
        let id = sqlx::query(
            r#"INSERT INTO otws(
                doctor_code,
                otw_date,
                otw_at,
                eta_at
                ) VALUES (?,?,?,?)"#,
        )
        .bind(doctor_code)
        .bind(otw_date)
        .bind(otw_at)
        .bind(eta_at)
        .execute(&self.pool)
        .await
        .context(format!(
            "error when assigning otw to doctor_code {doctor_code}"
        ))?
        .last_insert_id();

        let otw = sqlx::query_as::<_, Otw>(
            r#"SELECT 
                id,
                doctor_code,
                otw_date,
                otw_at,
                eta_at,
                arrive_at
            FROM otws 
            WHERE 
                id = ?"#,
        )
        .bind(id)
        .fetch_optional(&self.pool)
        .await
        .context("error when fetching created otws")?
        .ok_or(anyhow!("created otws is not found"))?;

        Ok(otw.into())
    }

    async fn get_otw_by_date_and_doctor(
        &self,
        otw_date: NaiveDate,
        doctor_code: &str,
    ) -> StorageServiceResult<ServiceOtw> {
        let otw = sqlx::query_as::<_, Otw>(
            r#"SELECT 
                id,
                doctor_code,
                otw_date,
                otw_at,
                eta_at,
                arrive_at
            FROM otws
            WHERE 
                otw_date = ?
                AND doctor_code = ?"#,
        )
        .bind(otw_date)
        .bind(doctor_code)
        .fetch_optional(&self.pool)
        .await
        .context("error when fetching created otws")?
        .map(Into::into)
        .ok_or(StorageServiceError::OtwDoctorNotFouned {
            doctor_code: String::from(doctor_code),
            otw_at: otw_date,
        })?;
        Ok(otw)
    }

    async fn cancel_otw(&self, otw_id: u32) -> StorageServiceResult<()> {
        let rows_affected = sqlx::query("DELETE FROM otws WHERE id = ?")
            .bind(otw_id)
            .execute(&self.pool)
            .await
            .context("error when canceling otw")?
            .rows_affected();

        if rows_affected == 0 {
            return Err(StorageServiceError::OtwIdNotFouned { id: otw_id });
        }
        Ok(())
    }

    async fn mark_arrive_otw(
        &self,
        _doctor_code: &str,
        _otw_date: NaiveDate,
    ) -> StorageServiceResult<()> {
        todo!()
    }
    async fn get_doctors_will_otw(
        &self,
        otw_at: chrono::DateTime<chrono::Local>,
        interval: i64,
    ) -> StorageServiceResult<Vec<hp2k_bot_core::DoctorOtw>> {
        let max = otw_at
            .with_second(0)
            .context("error when setting seconds to 0")?;
        let min = max
            .checked_sub_signed(Duration::minutes(interval))
            .context("error when setting min time")?;
        let doctors = sqlx::query_as::<_, DoctorOtw>(
            r#"SELECT 
                d.code,
                d.name,
                d.speciality,
                d.speciality_code ,
                o.otw_date,
                o.otw_at,
                o.eta_at
            FROM doctors d 
            INNER JOIN otws o 
            ON o.doctor_code = d.code
            WHERE 
                o.updated_at >= ? 
                AND o.updated_at <= ? 
                AND o.eta_at >= ?"#,
        )
        .bind(min)
        .bind(max)
        .bind(min)
        .bind(min)
        .fetch_all(&self.pool)
        .await
        .context("error when getting doctors")?;

        Ok(doctors.into_iter().map(Into::into).collect())
    }

    async fn get_doctors_will_arrive(
        &self,
        arrive_at: DateTime<chrono::Local>,
        interval: i64,
    ) -> StorageServiceResult<Vec<hp2k_bot_core::DoctorOtw>> {
        let max = arrive_at
            .with_second(0)
            .and_then(|e| e.checked_sub_signed(Duration::minutes(interval)))
            .context("error when setting seconds to 0")?;
        let min = max
            .checked_sub_signed(Duration::minutes(5))
            .context("error when setting min time")?;
        let doctors = sqlx::query_as::<_, DoctorOtw>(
            r#"SELECT 
                d.code,
                d.name,
                d.speciality,
                d.speciality_code 
            FROM doctors d 
            INNER JOIN otws o 
            ON o.doctor_code = d.code
            WHERE 
                o.eta_at >= ? 
                AND o.eta_at <= ?"#,
        )
        .bind(min)
        .bind(max)
        .bind(max)
        .fetch_all(&self.pool)
        .await
        .context("error when getting doctors")?;

        Ok(doctors.into_iter().map(Into::into).collect())
    }

    async fn get_registered_doctor(&self) -> StorageServiceResult<Vec<ServiceDoctor>> {
        let query = "SELECT code,name,speciality,speciality_code, chat_id,user_id FROM doctors WHERE chat_id IS NOT NULL and user_id IS NOT NULL";
        let result: Vec<Doctor> = sqlx::query_as(query)
            .fetch_all(&self.pool)
            .await
            .context("error when getting doctors")?;

        Ok(result.into_iter().map(Into::into).collect())
    }
    async fn get_doctor_schedule(
        &self,
        code: &str,
        schedule_date: NaiveDate,
    ) -> StorageServiceResult<hp2k_bot_core::Schedule> {
        let query = "SELECT id,doctor_code,schedule_date,schedule_at FROM schedules WHERE doctor_code = ? AND schedule_date = ?";
        let schedule: Schedule = sqlx::query_as(query)
            .bind(code)
            .bind(schedule_date)
            .fetch_optional(&self.pool)
            .await
            .context("error when getting doctor schedule")?
            .ok_or(StorageServiceError::ScheduleDateNotFound {
                doctor_code: String::from(code),
                schedule_date,
            })?;

        Ok(schedule.into())
    }

    async fn get_last_doctor_schedule(
        &self,
        code: &str,
        last_date: NaiveDate,
    ) -> StorageServiceResult<hp2k_bot_core::Schedule> {
        let from_date = last_date
            .checked_sub_days(chrono::Days::new(7))
            .expect("error setting date");
        let query =
            "SELECT id,doctor_code,schedule_date,schedule_at FROM schedules WHERE doctor_code = ? AND schedule_date >= ? AND schedule_date <= ? LIMIT 1";

        let result: Schedule = sqlx::query_as(query)
            .bind(code)
            .bind(from_date)
            .bind(last_date)
            .fetch_optional(&self.pool)
            .await
            .context("error when getting last doctor schedule")?
            .ok_or(StorageServiceError::SchedulePrevWeekNotFound {
                doctor_code: String::from(code),
                schedule_date: from_date,
            })?;

        Ok(result.into())
    }
    async fn set_schedule(
        &self,
        code: &str,
        schedule_date: NaiveDate,
        schedule_at: DateTime<chrono::prelude::Local>,
    ) -> StorageServiceResult<()> {
        let sql = "INSERT INTO schedules (doctor_code,schedule_date,schedule_at) VALUES (?,?,?)";

        sqlx::query(sql)
            .bind(code)
            .bind(schedule_date)
            .bind(schedule_at)
            .execute(&self.pool)
            .await
            .context("error when creating schedule")?;

        Ok(())
    }
}
