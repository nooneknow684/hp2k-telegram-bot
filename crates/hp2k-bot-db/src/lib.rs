use config::{ext::MemoryConfigurationBuilderExtensions, ConfigurationBuilder};
use di::injectable;
use options::Options;
use sqlx::mysql::{MySqlConnectOptions, MySqlPool};

#[cfg(test)]
use sqlx::mysql::MySqlPoolOptions;

pub use ext::WithMySqlSqlxDatabase;
use tracing::info;

mod helper;
mod schema;
mod task;
mod types;

#[cfg(test)]
mod test;

#[derive(Debug, serde::Deserialize)]
pub struct MySqlSqlxDatabaseOption {
    host: String,
    port: u16,
    username: String,
    password: String,
    database: String,
}

impl Default for MySqlSqlxDatabaseOption {
    fn default() -> Self {
        Self {
            port: 3306,
            host: String::from("localhost"),
            username: String::from("root"),
            password: String::from(""),
            database: String::from("otw-bot-rs"),
        }
    }
}

#[derive(Debug)]
pub struct MySqlSqlxDatabase {
    pool: MySqlPool,
}

#[injectable]
impl MySqlSqlxDatabase {
    pub fn new(setting: di::Ref<dyn Options<MySqlSqlxDatabaseOption>>) -> Self {
        let setting = setting.value();
        info!(
            "Initializing database service : mysql://{}@{}:{}/{}",
            setting.username, setting.host, setting.port, setting.database
        );
        let connection_option = MySqlConnectOptions::new()
            .host(&setting.host)
            .port(setting.port)
            .username(&setting.username)
            .password(&setting.password)
            .database(&setting.database);

        let connection = MySqlPool::connect_lazy_with(connection_option);

        Self { pool: connection }
    }

    #[cfg(test)]
    pub fn create(pool_opt: MySqlPoolOptions, conn_opt: MySqlConnectOptions) -> Self {
        let pool = pool_opt.connect_lazy_with(conn_opt);
        Self { pool }
    }

    #[cfg(test)]
    pub fn get_pool(&self) -> &MySqlPool {
        &self.pool
    }

    pub fn default_config() -> Box<dyn config::ConfigurationRoot> {
        config::DefaultConfigurationBuilder::new()
            .add_in_memory(&[
                ("database:mysql:host", "localhost"),
                ("database:mysql:username", "root"),
                ("database:mysql:password", ""),
                ("database:mysql:port", "3306"),
                ("database:mysql:database", "otw-bot-rs"),
            ])
            .build()
            .unwrap()
    }
}

mod ext {
    use std::sync::Arc;

    use config::ConfigurationRoot;
    use di::{transient_factory, Injectable, ServiceCollection};
    use hp2k_bot_core::{Migratable, StorageService, TaskStorageService};
    use options::ext::OptionsConfigurationServiceExtensions;

    use crate::{MySqlSqlxDatabase, MySqlSqlxDatabaseOption};

    pub trait WithMySqlSqlxDatabase {
        fn with_mysql_sqlx_database(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self;
    }

    impl WithMySqlSqlxDatabase for ServiceCollection {
        fn with_mysql_sqlx_database(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self {
            self.apply_config::<MySqlSqlxDatabaseOption>(
                config
                    .section("database")
                    .section("mysql")
                    .as_config()
                    .into(),
            );
            self.try_add(super::MySqlSqlxDatabase::singleton());

            self.try_add(transient_factory::<dyn StorageService, _>(|provider| {
                provider.get_required::<MySqlSqlxDatabase>()
            }));

            self.try_add(transient_factory::<dyn TaskStorageService, _>(|provider| {
                provider.get_required::<MySqlSqlxDatabase>()
            }));

            self.add(transient_factory::<dyn Migratable, _>(|provider| {
                provider.get_required::<MySqlSqlxDatabase>()
            }));

            self
        }
    }
}
