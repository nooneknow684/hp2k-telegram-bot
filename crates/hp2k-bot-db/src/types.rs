use chrono::{DateTime, Local, NaiveDate};
use sqlx::{mysql::MySqlRow, prelude::FromRow, Row};

#[derive(Debug)]
pub struct DoctorCode {
    pub old_id: Option<String>,
    pub new_id: Option<String>,
}

impl FromRow<'_, MySqlRow> for DoctorCode {
    fn from_row(row: &MySqlRow) -> Result<Self, sqlx::Error> {
        Ok({
            Self {
                old_id: row.try_get("old_id").ok(),
                new_id: row.try_get("new_id")?,
            }
        })
    }
}

#[derive(Debug, FromRow)]
pub struct Doctor {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,
    pub user_id: Option<u64>,
    pub chat_id: Option<i64>,
}

impl From<Doctor> for hp2k_bot_core::Doctor {
    fn from(
        Doctor {
            code,
            name,
            speciality,
            speciality_code,
            user_id,
            chat_id,
        }: Doctor,
    ) -> Self {
        hp2k_bot_core::Doctor {
            code,
            name,
            speciality,
            speciality_code,
            user_id,
            chat_id,
        }
    }
}

#[derive(Debug, FromRow)]
pub struct Otw {
    pub id: u32,
    pub doctor_code: String,
    pub otw_date: chrono::NaiveDate,
    pub otw_at: chrono::DateTime<chrono::Utc>,
    pub eta_at: chrono::DateTime<chrono::Utc>,
    pub arrive_at: Option<chrono::DateTime<chrono::Utc>>,
}

impl From<Otw> for hp2k_bot_core::Otw {
    fn from(
        Otw {
            id,
            doctor_code,
            otw_date,
            otw_at,
            eta_at,
            arrive_at,
        }: Otw,
    ) -> Self {
        Self {
            id,
            doctor_code,
            otw_date,
            otw_at,
            eta_at,
            arrive_at,
        }
    }
}

#[derive(Debug, FromRow)]
pub struct DoctorOtw {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,

    pub otw_date: chrono::NaiveDate,
    pub otw_at: chrono::DateTime<chrono::Utc>,
    pub eta_at: chrono::DateTime<chrono::Utc>,
}

impl From<DoctorOtw> for hp2k_bot_core::DoctorOtw {
    fn from(
        DoctorOtw {
            code,
            name,
            speciality,
            speciality_code,
            otw_date,
            otw_at,
            eta_at,
        }: DoctorOtw,
    ) -> Self {
        Self {
            code,
            name,
            speciality,
            speciality_code,
            otw_date,
            otw_at,
            eta_at,
        }
    }
}

#[derive(Debug, FromRow)]
pub struct Schedule {
    pub id: u32,
    pub doctor_code: String,
    pub schedule_date: NaiveDate,
    pub schedule_at: DateTime<Local>,
}

impl From<Schedule> for hp2k_bot_core::Schedule {
    fn from(
        Schedule {
            id,
            doctor_code,
            schedule_date,
            schedule_at,
        }: Schedule,
    ) -> Self {
        Self {
            id,
            doctor_code,
            schedule_date,
            schedule_at,
        }
    }
}
