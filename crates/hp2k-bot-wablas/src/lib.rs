use anyhow::Context;
use config::{
    ext::MemoryConfigurationBuilderExtensions, ConfigurationBuilder, ConfigurationRoot,
    DefaultConfigurationBuilder,
};
use di::injectable;
use hp2k_bot_core::MessageService;
use options::Options;
use reqwest::header::AUTHORIZATION;

pub use ext::*;

#[derive(Debug, Default, Clone, serde::Deserialize)]
pub struct WaBlasServiceOption {
    pub token: String,
    pub url: String,
}

pub struct WaBlasService {
    option: di::Ref<WaBlasServiceOption>,
}

#[injectable(MessageService)]
impl WaBlasService {
    pub fn new(opt: di::Ref<dyn Options<WaBlasServiceOption>>) -> Self {
        let option = opt.value();

        Self { option }
    }

    pub fn default_config() -> Box<dyn ConfigurationRoot> {
        DefaultConfigurationBuilder::new()
            // .add_json_file("./setting.json")
            .add_in_memory(&[
                (
                    "extra:message:wablas:url",
                    "mysql://root:tiitrsuGmc!@192.168.1.111/sik_dev",
                ),
                (
                    "extra:message:wablas:token",
                    "mysql://root:tiitrsuGmc!@192.168.1.111/sik_dev",
                ),
            ])
            .build()
            .unwrap()
    }
}

#[async_trait::async_trait]
impl MessageService for WaBlasService {
    async fn send_message(
        &self,
        message: hp2k_bot_core::Message,
    ) -> hp2k_bot_core::MessageServiceResult<()> {
        self.send_messages(&[message]).await?;
        Ok(())
    }
    async fn send_messages(
        &self,
        messages: &[hp2k_bot_core::Message],
    ) -> hp2k_bot_core::MessageServiceResult<()> {
        reqwest::Client::new()
            .post(format!("{}/v2/", self.option.url))
            .header(AUTHORIZATION, format!("bearer {}", self.option.token))
            .json(&serde_json::json!({ "data": messages }))
            .send()
            .await
            .context("error when requesting data")?;
        Ok(())
    }
}

mod ext {
    use std::sync::Arc;

    use config::ConfigurationRoot;
    use di::{Injectable, ServiceCollection};
    use options::ext::OptionsConfigurationServiceExtensions;

    pub trait WithWaBlasService {
        fn with_wablas_service(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self;
    }

    impl WithWaBlasService for ServiceCollection {
        fn with_wablas_service(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self {
            self.apply_config::<super::WaBlasServiceOption>(
                config
                    .section("extra")
                    .section("message")
                    .section("wablas")
                    .as_config()
                    .into(),
            );
            self.try_add(super::WaBlasService::singleton())
        }
    }
}
