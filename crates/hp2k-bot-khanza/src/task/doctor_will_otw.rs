use std::sync::Arc;

use anyhow::Context;
use hp2k_bot_core::{
    DoctorWillOtwPayload, Message, MessageService, StorageService, TaskPayloadParser,
    TaskPayloadRunner, TaskRegisterable,
};
use tracing::{instrument, warn};

use crate::KhanzaService;

pub struct KhanzaNotifyPatientDoctorWillOtw {
    db: Arc<dyn StorageService>,
    khanza: Arc<KhanzaService>,
    sender: Arc<dyn MessageService>,
}

impl TaskRegisterable for KhanzaNotifyPatientDoctorWillOtw {
    type Payload = DoctorWillOtwPayload;
    fn task_name() -> String {
        String::from("doctor-will-otw")
    }
    fn from_provider(provider: &di::ServiceProvider) -> Self {
        Self {
            db: provider.get_required(),
            khanza: provider.get_required(),
            sender: provider.get_required(),
        }
    }
}

impl TaskPayloadParser for KhanzaNotifyPatientDoctorWillOtw {
    type Payload = DoctorWillOtwPayload;
}

#[async_trait::async_trait]
impl TaskPayloadRunner for KhanzaNotifyPatientDoctorWillOtw {
    #[instrument(skip_all)]
    async fn run(&self, payload: String) -> anyhow::Result<()> {
        let payload = Self::from_payload(&payload)?;

        match self
            .db
            .get_otw_by_date_and_doctor(payload.otw_date, &payload.code)
            .await
        {
            Err(hp2k_bot_core::StorageServiceError::OtwDoctorNotFouned {
                doctor_code,
                otw_at,
            }) => {
                warn!(
                    "otw schedule for doctor {} at {} is not exists in database",
                    doctor_code,
                    otw_at.format("%Y-%m-%d")
                );
                return Ok(());
            }
            Err(err) => Err(err).context("error when getting otw schedule")?,
            _ => {}
        }

        let patients = self
            .khanza
            .get_patients(&payload.code, payload.otw_date)
            .await
            .context("error when getting patient data from khanza")?;

        if patients.is_empty() {
            warn!("no patients to be notified");
            return Ok(());
        }
        let messages: Vec<_> = patients
            .into_iter()
            .map(|patient| {
                Message::to(&patient.no_tlp).notify_patient_otw(
                    &patient.nm_pasien,
                    &payload.name,
                    payload.eta_at.into(),
                )
            })
            .collect();

        self.sender
            .send_messages(&messages)
            .await
            .context("error when sending messages to patients")?;

        Ok(())
    }
}
