use chrono::NaiveDate;
use config::{
    ext::MemoryConfigurationBuilderExtensions, ConfigurationBuilder, ConfigurationRoot,
    DefaultConfigurationBuilder,
};
use di::injectable;
use hp2k_bot_core::DoctorCreate;
use options::Options;
use sqlx::{prelude::FromRow, MySqlPool};

pub use ext::WithKhanzaService;

#[derive(Debug, FromRow)]
pub struct KhanzaDoctor {
    pub kd_dokter: String,
    pub nm_dokter: String,
    pub kd_sps: String,
    pub nm_sps: String,
}

#[derive(Debug, FromRow)]
pub struct KhanzaPatient {
    pub nm_pasien: String,
    pub no_tlp: String,
    pub kd_dokter: String,
    pub no_reg: String,
}

impl From<KhanzaDoctor> for DoctorCreate {
    fn from(v: KhanzaDoctor) -> Self {
        Self {
            code: v.kd_dokter,
            name: v.nm_dokter,
            speciality: v.nm_sps,
            speciality_code: v.kd_sps,
        }
    }
}

#[derive(Debug, Default, serde::Deserialize)]
pub struct KhanzaServiceOption {
    database_url: String,
}

pub struct KhanzaService {
    pool: MySqlPool,
}

#[injectable]
impl KhanzaService {
    pub fn new(opiton: di::Ref<dyn Options<KhanzaServiceOption>>) -> Self {
        let option = opiton.value();
        Self {
            pool: MySqlPool::connect_lazy(&option.database_url)
                .expect("error when connecting to khanza database"),
        }
    }

    pub async fn get_doctors(&self) -> anyhow::Result<Vec<KhanzaDoctor>> {
        let doctors = sqlx::query_as::<_, KhanzaDoctor>(
            r#"
            SELECT 
                d.kd_dokter,
                d.nm_dokter,
                s.kd_sps,
                s.nm_sps
            FROM dokter d
            INNER JOIN spesialis s
            ON s.kd_sps = d.kd_sps
        "#,
        )
        .fetch_all(&self.pool)
        .await?;

        Ok(doctors)
    }
    pub async fn get_patients(
        &self,
        doctor_code: &str,
        reg_date: NaiveDate,
    ) -> anyhow::Result<Vec<KhanzaPatient>> {
        let query = r#" SELECT
                        p.nm_pasien,
                        p.no_tlp,
                        r.kd_dokter,
                        r.no_reg FROM pasien p
                        INNER JOIN reg_periksa r
                        ON r.no_rkm_medis = p.no_rkm_medis
                        WHERE 
                            r.tgl_registrasi = ?
                            AND r.kd_dokter = ? 
                        ORDER BY r.no_reg ASC
                        LIMIT 10
                "#;

        let result = sqlx::query_as(query)
            .bind(reg_date)
            .bind(doctor_code)
            .fetch_all(&self.pool)
            .await?;

        Ok(result)
    }

    pub fn default_config() -> Box<dyn ConfigurationRoot> {
        DefaultConfigurationBuilder::new()
            .add_in_memory(&[(
                "extra:khanza:database_url",
                "mysql://root:tiitrsuGmc!@192.168.1.111/sik_dev",
            )])
            .build()
            .unwrap()
    }
}

pub mod ext {
    use std::sync::Arc;

    use config::ConfigurationRoot;
    use di::Injectable;
    use di::ServiceCollection;
    use options::ext::OptionsConfigurationServiceExtensions;

    pub trait WithKhanzaService {
        fn with_khanza_service(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self;
    }

    impl WithKhanzaService for ServiceCollection {
        fn with_khanza_service(&mut self, config: &Arc<dyn ConfigurationRoot>) -> &mut Self {
            self.apply_config::<super::KhanzaServiceOption>(
                config.section("extra").section("khanza").as_config().into(),
            );
            self.try_add(super::KhanzaService::singleton())
        }
    }
}
