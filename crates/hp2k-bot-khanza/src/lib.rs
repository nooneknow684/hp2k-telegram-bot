mod service;
mod task;

pub use service::{KhanzaDoctor, KhanzaPatient, KhanzaService, WithKhanzaService};
pub use task::*;
