#[async_trait::async_trait]
pub trait Migratable {
    async fn migrate(&self) -> anyhow::Result<()>;
}
