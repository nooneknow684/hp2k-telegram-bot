use crate::{PayloadBuilderWrapper, TaskPayload, TaskPayloadBuilder};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DoctorWillArrivePayload {
    pub code: String,
    pub name: String,
    pub speciality: String,

    pub otw_id: u32,
    pub otw_date: chrono::NaiveDate,
    pub otw_at: chrono::DateTime<chrono::Local>,
    pub eta_at: chrono::DateTime<chrono::Local>,
}

impl TaskPayload for DoctorWillArrivePayload {
    type Builder = DoctorWillArrivePayloadBuilder;
    fn builder() -> Self::Builder {
        Self::Builder::default()
    }
    fn task_name() -> &'static str {
        "doctor-will-arrive"
    }
}

#[derive(Default)]
pub struct DoctorWillArrivePayloadBuilder {
    code: Option<String>,
    name: Option<String>,
    speciality: Option<String>,

    otw_id: Option<u32>,
    otw_date: Option<chrono::NaiveDate>,
    otw_at: Option<chrono::DateTime<chrono::Local>>,
    eta_at: Option<chrono::DateTime<chrono::Local>>,
}

impl TaskPayloadBuilder for DoctorWillArrivePayloadBuilder {
    type Target = DoctorWillArrivePayload;
    fn into_target(self) -> Self::Target {
        Self::Target {
            code: self.code.unwrap(),
            name: self.name.unwrap(),
            speciality: self.speciality.unwrap(),

            otw_id: self.otw_id.unwrap(),
            otw_date: self.otw_date.unwrap(),
            otw_at: self.otw_at.unwrap(),
            eta_at: self.eta_at.unwrap(),
        }
    }
}

impl PayloadBuilderWrapper<DoctorWillArrivePayload> {
    pub fn set_doctor(mut self, code: &str, name: &str, speciality: &str) -> Self {
        self.builder.code = Some(code.into());
        self.builder.name = Some(name.into());
        self.builder.speciality = Some(speciality.into());
        self
    }

    pub fn set_otw(
        mut self,
        otw_id: u32,
        otw_date: chrono::NaiveDate,
        otw_at: chrono::DateTime<chrono::Local>,
        eta_at: chrono::DateTime<chrono::Local>,
    ) -> Self {
        self.builder.otw_id = Some(otw_id);
        self.builder.otw_date = Some(otw_date);
        self.builder.otw_at = Some(otw_at);
        self.builder.eta_at = Some(eta_at);
        self
    }
}
