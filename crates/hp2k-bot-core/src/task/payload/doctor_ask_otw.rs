use crate::{PayloadBuilderWrapper, TaskPayload, TaskPayloadBuilder};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DoctorAskOtwPayload {
    pub code: String,
}

impl TaskPayload for DoctorAskOtwPayload {
    type Builder = DoctorAskOtwPayloadBuilder;
    fn task_name() -> &'static str {
        "doctor-ask-otw"
    }
}

#[derive(Default)]
pub struct DoctorAskOtwPayloadBuilder {
    pub code: Option<String>,
}

impl TaskPayloadBuilder for DoctorAskOtwPayloadBuilder {
    type Target = DoctorAskOtwPayload;
    fn into_target(self) -> Self::Target {
        Self::Target {
            code: self.code.unwrap(),
        }
    }
}
impl PayloadBuilderWrapper<DoctorAskOtwPayload> {
    pub fn set_doctor(mut self, doctor_code: &str) -> Self {
        self.builder.code = Some(doctor_code.into());
        self
    }
}
