use crate::{
    Task, TaskDispatcherBuilder, TaskDispatcherDependencies, TaskDispatcherExecuteAtTime,
    TaskStorageService,
};
use serde::Serialize;
use std::sync::Arc;

mod doctor_arrive;
mod doctor_ask_otw;
mod doctor_ask_schedule;
mod doctor_will_arrive;
mod doctor_will_otw;

pub use doctor_arrive::*;
pub use doctor_ask_otw::*;
pub use doctor_ask_schedule::*;
pub use doctor_will_arrive::*;
pub use doctor_will_otw::*;

pub trait TaskPayload {
    type Builder: 'static + Default + TaskPayloadBuilder<Target = Self>;
    fn task_name() -> &'static str;
    fn default_execute_at() -> chrono::DateTime<chrono::Local> {
        chrono::Local::now()
    }
    fn builder() -> Self::Builder {
        Self::Builder::default()
    }
}

pub trait TaskPayloadBuilder {
    type Target: 'static + Send + Sync + TaskPayload + Serialize;
    fn into_target(self) -> Self::Target;
}

pub struct PayloadBuilderWrapper<Payload>
where
    Payload: 'static + Sync + Send + TaskPayload,
{
    pub(crate) dep: TaskDispatcherDependencies,
    pub(crate) builder: Payload::Builder,
}

impl<T> PayloadBuilderWrapper<T>
where
    T: 'static + Send + Sync + serde::Serialize + TaskPayload,
{
    pub fn new(db: Arc<dyn TaskStorageService>) -> Self {
        Self {
            dep: TaskDispatcherDependencies::new(db),
            builder: T::builder(),
        }
    }

    pub fn execute_at(
        self,
        execute_at: chrono::DateTime<chrono::Local>,
    ) -> TaskDispatcherBuilder<T, TaskDispatcherExecuteAtTime> {
        TaskDispatcherBuilder {
            dep: self.dep,
            payload: self.builder.into_target(),
            execute_at: TaskDispatcherExecuteAtTime(execute_at),
        }
    }

    pub async fn dispatch(self) -> anyhow::Result<Task> {
        let dispatcher = TaskDispatcherBuilder {
            dep: self.dep,
            payload: self.builder.into_target(),
            execute_at: TaskDispatcherExecuteAtTime(T::default_execute_at()),
        };
        dispatcher.dispatch().await
    }
}
