use chrono::Timelike;

use crate::{PayloadBuilderWrapper, TaskPayload, TaskPayloadBuilder};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DoctorAskSchedulePayload {
    pub code: String,
}

impl TaskPayload for DoctorAskSchedulePayload {
    type Builder = DoctorAskSchedulePayloadBuilder;

    fn default_execute_at() -> chrono::DateTime<chrono::Local> {
        chrono::Local::now()
            .with_hour(18)
            .expect("error when setting hour")
            .with_minute(0)
            .expect("error when setting minute")
            .with_second(0)
            .expect("error when setting second")
            .with_nanosecond(0)
            .expect("error when setting subsecond")
    }

    fn task_name() -> &'static str {
        "doctor-ask-schedule"
    }
}

#[derive(Default)]
pub struct DoctorAskSchedulePayloadBuilder {
    pub code: Option<String>,
}

impl TaskPayloadBuilder for DoctorAskSchedulePayloadBuilder {
    type Target = DoctorAskSchedulePayload;
    fn into_target(self) -> Self::Target {
        Self::Target {
            code: self.code.unwrap(),
        }
    }
}

impl PayloadBuilderWrapper<DoctorAskSchedulePayload> {
    pub fn set_doctor(mut self, doctor_code: &str) -> Self {
        self.builder.code = Some(doctor_code.into());
        self
    }
}
