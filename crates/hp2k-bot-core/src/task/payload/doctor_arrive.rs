use crate::{TaskPayload, TaskPayloadBuilder};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DoctorArrivePayload {
    pub code: String,
    pub name: String,
    pub speciality: String,

    pub otw_id: String,
    pub arrive_at: String,
}

impl TaskPayload for DoctorArrivePayload {
    type Builder = DoctorArrivePayloadBuilder;
    fn task_name() -> &'static str {
        "doctor-arrive"
    }

    fn builder() -> Self::Builder {
        Self::Builder::default()
    }
}

#[derive(Default)]
pub struct DoctorArrivePayloadBuilder {
    code: Option<String>,
    name: Option<String>,
    speciality: Option<String>,

    otw_id: Option<String>,
    arrive_at: Option<String>,
}

impl TaskPayloadBuilder for DoctorArrivePayloadBuilder {
    type Target = DoctorArrivePayload;
    fn into_target(self) -> Self::Target {
        Self::Target {
            code: self.code.unwrap(),
            name: self.name.unwrap(),
            speciality: self.speciality.unwrap(),

            otw_id: self.otw_id.unwrap(),
            arrive_at: self.arrive_at.unwrap(),
        }
    }
}
