use crate::Task;

#[derive(Debug, thiserror::Error)]
pub enum TaskStorageError {
    #[error("error when getting tasks : {}", _0)]
    GetTasksFailed(anyhow::Error),
    #[error("error when creating task : {}", _0)]
    CreateTaskFailed(anyhow::Error),
    #[error("error when deleting task : {}", _0)]
    DeleteTaskFaield(anyhow::Error),
    #[error("task with id {} not exists in database", _0)]
    TaskNotFound(String),
    #[error("error when serde  : {}", _0)]
    Deserialize(#[from] serde_json::Error),
    #[error("unknown error : {}", _0)]
    Unknown(#[from] anyhow::Error),
}

impl TaskStorageError {
    pub fn create_task_failed(err: impl Into<anyhow::Error>) -> Self {
        Self::CreateTaskFailed(err.into())
    }
    pub fn delete_task_failed(err: impl Into<anyhow::Error>) -> Self {
        Self::DeleteTaskFaield(err.into())
    }
    pub fn get_tasks_failed(err: impl Into<anyhow::Error>) -> Self {
        Self::GetTasksFailed(err.into())
    }
}

pub type TaskStorageResult<T> = Result<T, TaskStorageError>;

#[async_trait::async_trait]
pub trait TaskStorageService: Send + Sync {
    async fn create_task(
        &self,
        task_name: &str,
        payload: &str,
        execute_at: chrono::DateTime<chrono::Local>,
    ) -> TaskStorageResult<Task>;
    async fn mark_task_executed(&self, task_id: uuid::Uuid) -> TaskStorageResult<()>;
    async fn get_tasks(
        &self,
        execute_at: chrono::DateTime<chrono::Local>,
    ) -> TaskStorageResult<Vec<Task>>;
    async fn cancel_task(&self, id: &str) -> TaskStorageResult<()>;
}
