mod dispatcher;
mod payload;
mod runner;
mod storage;
mod types;

use std::sync::Arc;

use di::injectable;

pub use dispatcher::*;
pub use ext::*;
pub use payload::*;
pub use runner::*;
pub use storage::*;
pub use types::*;

#[injectable]
pub struct BaseTaskDispatcher {
    db: Arc<dyn TaskStorageService>,
}

impl BaseTaskDispatcher {
    pub fn doctor_will_otw(&self) -> PayloadBuilderWrapper<DoctorWillOtwPayload> {
        TaskDispatcherBuilder::init(&self.db)
    }
    pub fn doctor_will_arrive(&self) -> PayloadBuilderWrapper<DoctorWillArrivePayload> {
        TaskDispatcherBuilder::init(&self.db)
    }
    pub fn doctor_arrive(&self) -> PayloadBuilderWrapper<DoctorArrivePayload> {
        TaskDispatcherBuilder::init(&self.db)
    }
    pub fn doctor_ask_schedule(&self) -> PayloadBuilderWrapper<DoctorAskSchedulePayload> {
        TaskDispatcherBuilder::init(&self.db)
    }
    pub fn doctor_ask_otw(&self) -> PayloadBuilderWrapper<DoctorAskOtwPayload> {
        TaskDispatcherBuilder::init(&self.db)
    }
}

mod ext {
    use di::{Injectable, ServiceCollection};

    use crate::BaseTaskDispatcher;

    pub trait WithBaseTaskDispatcher {
        fn with_base_task_dispatcher(&mut self) -> &mut Self;
    }

    impl WithBaseTaskDispatcher for ServiceCollection {
        fn with_base_task_dispatcher(&mut self) -> &mut Self {
            self.try_add(BaseTaskDispatcher::singleton())
        }
    }
}
