use std::sync::Arc;

use crate::{PayloadBuilderWrapper, Task, TaskPayload, TaskStorageService};

pub struct TaskDispatcherDependencies {
    db: Arc<dyn TaskStorageService>,
}

impl TaskDispatcherDependencies {
    pub fn new(db: Arc<dyn TaskStorageService>) -> Self {
        Self { db }
    }
}

pub struct TaskDispatcherExecuteAtNone;
pub struct TaskDispatcherExecuteAtTime(pub(crate) chrono::DateTime<chrono::Local>);

pub struct TaskDispatcherBuilder<Payload, ExecutedAt>
where
    Payload: 'static + Send + Sync + TaskPayload,
{
    pub(crate) dep: TaskDispatcherDependencies,
    pub(crate) payload: Payload,
    pub(crate) execute_at: ExecutedAt,
}

impl<'a, Payload> TaskDispatcherBuilder<Payload, TaskDispatcherExecuteAtNone>
where
    Payload: 'static + Send + Sync + TaskPayload + serde::Serialize,
{
    pub fn init(db: &'a Arc<dyn TaskStorageService>) -> PayloadBuilderWrapper<Payload> {
        PayloadBuilderWrapper::new(db.clone())
    }
}

impl<'a, Payload> TaskDispatcherBuilder<Payload, TaskDispatcherExecuteAtTime>
where
    Payload: 'static + Send + Sync + TaskPayload + serde::Serialize,
{
    pub async fn dispatch(self) -> anyhow::Result<Task> {
        let db = &self.dep.db;
        let task = db
            .create_task(
                Payload::task_name(),
                &serde_json::to_string(&self.payload)?,
                self.execute_at.0,
            )
            .await?;
        Ok(task)
    }
}
