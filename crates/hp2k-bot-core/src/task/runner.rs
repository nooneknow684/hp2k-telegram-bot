use std::{collections::HashMap, sync::Arc};

use anyhow::Context;
use di::ServiceProvider;
use serde::de::DeserializeOwned;
use tokio::task::JoinSet;
use tracing::{error, info, instrument, warn};
use uuid::Uuid;

use crate::{Task, TaskPayload, TaskStorageService};
#[async_trait::async_trait]
pub trait TaskPayloadRunner {
    async fn run(&self, payload: String) -> anyhow::Result<()>;
}

pub trait TaskRegisterable {
    type Payload: 'static + Send + Sync + DeserializeOwned + serde::Serialize + TaskPayload;
    fn from_provider(provider: &ServiceProvider) -> Self;
    fn task_name() -> String {
        String::from(Self::Payload::task_name())
    }
}

pub trait TaskPayloadParser {
    type Payload: 'static + Send + Sync + DeserializeOwned + serde::Serialize + TaskPayload;
    fn from_payload(payload: &str) -> anyhow::Result<Self::Payload> {
        serde_json::from_str(payload).context("error when parsing payload")
    }
    fn to_payload(val: Self::Payload) -> anyhow::Result<String> {
        serde_json::to_string(&val).context("error when serializing payload")
    }
}

pub struct TaskRunner {
    pub map: HashMap<String, Vec<Arc<dyn 'static + Send + Sync + TaskPayloadRunner>>>,
    pub provider: ServiceProvider,
}

impl TaskRunner {
    pub fn new(provider: &ServiceProvider) -> Self {
        Self {
            provider: provider.clone(),
            map: HashMap::default(),
        }
    }
    pub fn register_handler<Runner>(&mut self)
    where
        Runner: 'static + Send + Sync + TaskPayloadRunner + TaskRegisterable,
    {
        self.map
            .entry(Runner::task_name())
            .and_modify(|item| item.push(Arc::new(Runner::from_provider(&self.provider))))
            .or_insert(vec![Arc::new(Runner::from_provider(&self.provider))]);
    }

    pub async fn run(self) {
        let db = self.provider.get_required::<dyn TaskStorageService>();

        let (sender, mut receiver) = tokio::sync::mpsc::channel::<Vec<Task>>(10);

        tokio::join!(
            fetch_data(sender, db.clone()),
            task_runner(&mut receiver, self.map, db)
        );
    }
}

#[instrument(skip_all)]
async fn fetch_data(sender: tokio::sync::mpsc::Sender<Vec<Task>>, db: Arc<dyn TaskStorageService>) {
    let mut delay = 1000;

    loop {
        let now = chrono::Local::now();
        info!(
            "getting task to be executed at {}",
            now.format("%Y/%m/%d %H:%M:%S")
        );

        let tasks = match db.get_tasks(now).await {
            Ok(tasks) => Some(tasks),
            Err(err) => {
                error!(%err,"error when getting tasks from database");
                None
            }
        };
        info!("executing task at {}", now.format("%Y/%m/%d %H:%M:%S"));

        match tasks {
            Some(tasks) if tasks.is_empty() => {
                info!("no tasks to be executed");
                delay = std::cmp::min(10_000, delay + 1000);
            }
            Some(tasks) => {
                if let Err(err) = sender.send(tasks).await {
                    error!(%err,"error when sending tasks to runner threads");
                    delay = std::cmp::min(10_000, delay + 1000);
                }
            }
            None => {
                delay = std::cmp::min(10_000, delay + 1000);
            }
        };

        tokio::time::sleep(std::time::Duration::from_millis(delay)).await;
    }
}

async fn task_runner(
    receiver: &mut tokio::sync::mpsc::Receiver<Vec<Task>>,
    hashmap: HashMap<String, Vec<Arc<dyn 'static + Send + Sync + TaskPayloadRunner>>>,
    db: Arc<dyn TaskStorageService>,
) {
    loop {
        let tasks = match receiver.recv().await {
            Some(task) => task,
            None => {
                info!("sender disconnected");
                return;
            }
        };

        let mut jt = JoinSet::<Uuid>::new();

        for task in tasks {
            let task = task.clone();
            let executors = match hashmap.get(&task.name) {
                Some(res) if !res.is_empty() => res.clone(),
                _ => {
                    warn!(
                        "task runner(s) for '{}' tasks doesn't not exists",
                        task.name
                    );
                    continue;
                }
            };

            info!("executing task {} : {}", task.id, task.payload);

            jt.spawn(async move {
                task_executor(executors, &task).await;
                task.id
            });
        }

        while let Some(pool_result) = jt.join_next().await {
            match pool_result {
                Err(err) => error!(%err,"error when polling task"),
                Ok(id) => db
                    .mark_task_executed(id)
                    .await
                    .context("error when marking task as executed")
                    .unwrap(),
            };
        }
    }
}
async fn task_executor(
    runners: Vec<Arc<dyn 'static + Send + Sync + TaskPayloadRunner>>,
    task: &Task,
) {
    let mut jt = JoinSet::<anyhow::Result<()>>::new();

    if runners.is_empty() {
        warn!("task handler(s) for task '{}' is empty", task.name);
        return;
    }

    for runner in runners.iter() {
        let payload = String::from(&task.payload);
        let runner = runner.clone();

        jt.spawn(async move { runner.run(payload).await });
    }
    while let Some(pool_result) = jt.join_next().await {
        match pool_result {
            Err(err) => error!(%err,"error when polling task"),
            Ok(Err(err)) => error!(%err,"error when running task"),
            _ => {}
        };
    }
}
