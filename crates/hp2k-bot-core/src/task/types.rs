use chrono::{DateTime, Utc};

#[derive(Debug, Clone)]
pub struct Task {
    pub id: uuid::Uuid,
    pub name: String,
    pub execute_at: DateTime<Utc>,
    pub executed_at: Option<DateTime<Utc>>,
    pub payload: String,
}
