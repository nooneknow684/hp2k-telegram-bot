use crate::Message;

#[derive(Debug, thiserror::Error)]
pub enum MessageServiceError {
    #[error("unkown error : _0")]
    Unknown(#[from] anyhow::Error),
}

pub type MessageServiceResult<T> = Result<T, MessageServiceError>;

#[async_trait::async_trait]
pub trait MessageService: Send + Sync {
    async fn send_message(&self, message: Message) -> MessageServiceResult<()>;
    async fn send_messages(&self, messages: &[Message]) -> MessageServiceResult<()>;
}
