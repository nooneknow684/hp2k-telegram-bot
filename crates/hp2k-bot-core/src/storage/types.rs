use chrono::{DateTime, Local, NaiveDate, Utc};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CountedList<T> {
    pub data: Vec<T>,
    pub count: i64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Doctor {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,

    pub user_id: Option<u64>,
    pub chat_id: Option<i64>,
}

impl Doctor {
    pub fn code(self) -> String {
        self.code
    }
}

#[derive(Debug, Clone)]
pub struct DoctorOtw {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,

    pub otw_date: NaiveDate,
    pub otw_at: DateTime<Utc>,
    pub eta_at: DateTime<Utc>,
}

#[derive(Debug, Clone)]
pub struct DoctorCreate {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,
}

#[derive(Debug, Clone)]
pub struct Otw {
    pub id: u32,
    pub doctor_code: String,
    pub otw_date: NaiveDate,

    pub otw_at: DateTime<Utc>,
    pub eta_at: DateTime<Utc>,
    pub arrive_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Clone)]
pub struct Schedule {
    pub id: u32,
    pub doctor_code: String,
    pub schedule_date: NaiveDate,
    pub schedule_at: DateTime<Local>,
}

#[derive(Debug, Clone, serde::Serialize)]
pub struct Message {
    pub phone: String,
    pub message: String,
}

impl Message {
    pub fn to(phone: &str) -> MessageBuilder {
        MessageBuilder(String::from(phone))
    }
}

pub struct MessageBuilder(String);

impl MessageBuilder {
    pub fn new(phone: &str) -> Self {
        Self(String::from(phone))
    }

    pub fn notify_patient_otw(
        self,
        patient_name: &str,
        doctor_name: &str,
        eta_at: chrono::DateTime<chrono::Utc>,
    ) -> Message {
        let message = format!(
            "Halo {patient_name}, {doctor_name} akan tiba di rumah sakit jam {}",
            eta_at.format("%H:%M")
        );
        Message {
            phone: self.0,
            message,
        }
    }

    pub fn notify_patient_arrive(
        self,
        patient_name: &str,
        doctor_name: &str,
        interval: u32,
    ) -> Message {
        let message = format!(
        "Halo {patient_name}, {doctor_name} akan tiba di rumah sakit {interval} menit lagi ya. Terima kasih sudah menunggu"
    );
        Message {
            phone: self.0,
            message,
        }
    }
}
