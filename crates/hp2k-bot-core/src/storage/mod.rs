use async_trait::async_trait;
use chrono::{DateTime, Local, NaiveDate, Utc};

mod types;

pub use types::*;

pub type StorageServiceResult<T> = Result<T, StorageServiceError>;

#[derive(Debug, thiserror::Error)]
pub enum StorageServiceError {
    #[error("Doctor with code {doctor_code} is not exists in database")]
    DoctorCodeNotFound { doctor_code: String },
    #[error("Doctor with user_id {user_id} is not exists in database")]
    DoctorUserNotFound { user_id: u64 },
    #[error("Doctor with chat_id {chat_id} is not exists in database")]
    DoctorChatNotFound { chat_id: i64 },
    #[error("OTW schedule with id {id} is not exists in database")]
    OtwIdNotFouned { id: u32 },
    #[error("OTW schedule of doctor {doctor_code} at {otw_at} is not exists in database")]
    OtwDoctorNotFouned {
        doctor_code: String,
        otw_at: chrono::NaiveDate,
    },
    #[error("Doctor {doctor_code} for {schedule_date} is not exists")]
    ScheduleDateNotFound {
        doctor_code: String,
        schedule_date: NaiveDate,
    },
    #[error("Doctor {doctor_code} for {schedule_date} is not exists")]
    SchedulePrevWeekNotFound {
        doctor_code: String,
        schedule_date: NaiveDate,
    },
    #[error("User with chat_id = {chat_id} and user_id = {user_id} is not exists in database")]
    UserNotFound { chat_id: i64, user_id: u64 },
    #[error("Failed when running database migration")]
    Migration,
    #[error("Unkown storage service error : {0}")]
    Unknown(#[from] anyhow::Error),
}

#[async_trait]
pub trait StorageService: Send + Sync {
    async fn sync_doctors(&self, doctors: Vec<DoctorCreate>) -> StorageServiceResult<()>;

    async fn get_doctor(&self, doctor_code: &str) -> StorageServiceResult<Doctor>;
    async fn get_doctors(
        &self,
        doctor_name: &str,
        page: i64,
    ) -> StorageServiceResult<CountedList<Doctor>>;
    async fn get_doctor_by_user_id(&self, user_id: u64) -> StorageServiceResult<Doctor>;
    async fn get_doctor_by_chat_id(&self, chat_id: i64) -> StorageServiceResult<Doctor>;

    async fn get_user(&self, user_id: u32) -> StorageServiceResult<bool>;

    async fn map_doctor(
        &self,
        doctor_code: &str,
        user_id: u64,
        chat_id: i64,
    ) -> StorageServiceResult<()>;

    async fn unmap_user(&self, user_id: u64, chat_id: i64) -> StorageServiceResult<()>;

    async fn assign_otw(
        &self,
        doctor_code: &str,
        otw_date: NaiveDate,
        otw_at: DateTime<Utc>,
        eta_at: DateTime<Utc>,
    ) -> StorageServiceResult<Otw>;
    async fn cancel_otw(&self, otw_id: u32) -> StorageServiceResult<()>;
    async fn get_otw_by_date_and_doctor(
        &self,
        otw_date: NaiveDate,
        doctor_code: &str,
    ) -> StorageServiceResult<Otw>;

    async fn mark_arrive_otw(
        &self,
        doctor_code: &str,
        otw_date: NaiveDate,
    ) -> StorageServiceResult<()>;

    async fn get_doctors_will_otw(
        &self,
        otw_at: chrono::DateTime<Local>,
        interval: i64,
    ) -> StorageServiceResult<Vec<DoctorOtw>>;
    async fn get_doctors_will_arrive(
        &self,
        arrive_at: chrono::DateTime<Local>,
        interval: i64,
    ) -> StorageServiceResult<Vec<DoctorOtw>>;
    async fn get_registered_doctor(&self) -> StorageServiceResult<Vec<Doctor>>;

    async fn get_doctor_schedule(
        &self,
        code: &str,
        schedule_date: NaiveDate,
    ) -> StorageServiceResult<Schedule>;
    async fn get_last_doctor_schedule(
        &self,
        code: &str,
        last_date: NaiveDate,
    ) -> StorageServiceResult<Schedule>;
    async fn set_schedule(
        &self,
        code: &str,
        schedule_date: NaiveDate,
        schedule_at: DateTime<Local>,
    ) -> StorageServiceResult<()>;
}
