use clap::Parser;
use command::Cli;

mod command;
mod util;

#[tokio::main]
#[tracing::instrument(skip_all)]
async fn main() -> anyhow::Result<()> {
    util::init_subscriber();

    let args = Cli::parse();

    let provider = util::build_provider().await?;
    args.command.run(&provider).await;

    Ok(())
}
