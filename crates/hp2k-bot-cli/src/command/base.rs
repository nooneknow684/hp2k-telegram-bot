use di::ServiceProvider;
use hp2k_bot_core::{Migratable, TaskPayloadRunner, TaskRegisterable, TaskRunner};
use hp2k_bot_khanza::{KhanzaNotifyPatientDoctorWillArrive, KhanzaNotifyPatientDoctorWillOtw};
use hp2k_bot_teloxide::{DoctorAskOtwTaskRunner, DoctorAskScheduleTaskRunner, TeloxideDispatcher};
use tracing::{error, info};

#[derive(Debug, clap::Subcommand, Clone)]
pub(crate) enum BaseCommand {
    /// Run telegram bot
    Run {
        #[arg(short, long)]
        /// Use polling instead of webhook
        polling: bool,
    },
    /// Migrate database
    Migrate,
    /// Background task runner
    TaskRunner,
    /// DemoTaskRun
    DemoTask,
}

impl BaseCommand {
    pub async fn run(&self, provider: &ServiceProvider) {
        match self {
            Self::Run { polling } => run_handler(provider, *polling).await,
            Self::Migrate => migrate_handler(provider).await,
            Self::TaskRunner => task_runner_handler(provider).await,
            Self::DemoTask => demo_task(provider).await,
        }
    }
}

async fn task_runner_handler(provider: &ServiceProvider) {
    let mut task_runner = TaskRunner::new(provider);

    task_runner.register_handler::<KhanzaNotifyPatientDoctorWillOtw>();
    task_runner.register_handler::<KhanzaNotifyPatientDoctorWillArrive>();
    task_runner.register_handler::<DoctorAskScheduleTaskRunner>();
    task_runner.register_handler::<DoctorAskOtwTaskRunner>();

    task_runner.run().await
}

async fn migrate_handler(provider: &ServiceProvider) {
    let migratable: Vec<_> = provider.get_all::<dyn Migratable>().collect();
    for m in migratable {
        if let Err(err) = m.migrate().await {
            error!(%err,"error when running migration")
        };
    }
}

async fn run_handler(provider: &ServiceProvider, with_polling: bool) {
    if with_polling {
        info!("Running bot with polling");
        provider
            .get_required::<TeloxideDispatcher>()
            .start_polling()
            .await
    } else {
        info!("Running bot with webhook");
        provider
            .get_required::<TeloxideDispatcher>()
            .start_webhook()
            .await
    }
}

async fn demo_task(provider: &ServiceProvider) {
    DoctorAskScheduleTaskRunner::from_provider(provider)
        .run(String::from(r#"{"code":"D0000033"}"#))
        .await
        .unwrap();
}
