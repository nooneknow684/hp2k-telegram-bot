use di::ServiceProvider;

mod base;
mod khanza;

#[derive(clap::Parser, Debug)]
#[command(author, version, about)]
pub(crate) struct Cli {
    #[command(subcommand)]
    pub command: MyCommand,
}

#[derive(Debug, clap::Subcommand, Clone)]
pub(crate) enum MyCommand {
    #[command(flatten)]
    Khanza(khanza::KhanzaCommand),
    #[command(flatten)]
    Base(base::BaseCommand),
}

impl MyCommand {
    pub async fn run(&self, provider: &ServiceProvider) {
        match self {
            Self::Khanza(item) => item.run(provider).await,
            Self::Base(item) => item.run(provider).await,
        }
    }
}
