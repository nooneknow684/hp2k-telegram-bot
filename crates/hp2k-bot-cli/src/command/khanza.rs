use di::ServiceProvider;
use hp2k_bot_core::StorageService;
use hp2k_bot_khanza::KhanzaService;

#[derive(Debug, clap::Subcommand, Clone)]
pub(crate) enum KhanzaCommand {
    #[command(name = "khanza:sync-doctor")]
    /// Sync doctor with khanza doctors
    SyncDoctor,
}

impl KhanzaCommand {
    pub async fn run(&self, provider: &ServiceProvider) {
        match self {
            Self::SyncDoctor => khanza_sync_handler(provider).await,
        }
    }
}

async fn khanza_sync_handler(provider: &ServiceProvider) {
    let doctors = provider
        .get_required::<KhanzaService>()
        .get_doctors()
        .await
        .expect("error when getting doctors from khanza");
    provider
        .get_required::<dyn StorageService>()
        .sync_doctors(doctors.into_iter().map(Into::into).collect())
        .await
        .expect("error when syncing doctor");
}
