use anyhow::Context;
use di::ServiceProvider;
use std::{path::PathBuf, sync::Arc};
use tracing::level_filters::LevelFilter;

use hp2k_bot_core::WithBaseTaskDispatcher;
use hp2k_bot_khanza::{KhanzaService, WithKhanzaService};
use hp2k_bot_teloxide::{TeloxideDispatcher, WithTeloxideDispatcher};
use hp2k_bot_wablas::{WaBlasService, WithWaBlasService};

use config::{
    ext::{ChainedBuilderExtensions, FileSourceBuilderExtensions, JsonConfigurationExtensions},
    ConfigurationBuilder, DefaultConfigurationBuilder,
};
use di::ServiceCollection;
use hp2k_bot_db::{MySqlSqlxDatabase, WithMySqlSqlxDatabase};

pub fn init_subscriber() {
    tracing_subscriber::fmt()
        .with_max_level(LevelFilter::INFO)
        .init();
}

pub fn config_file() -> anyhow::Result<PathBuf> {
    let mut config_path = dirs::config_dir().context("error when getting config directory")?;
    config_path.push("hp2k-bot");
    config_path.push("appsetting.json");
    Ok(config_path)
}

pub async fn build_provider() -> anyhow::Result<ServiceProvider> {
    let config_builder = DefaultConfigurationBuilder::new()
        .add_configuration(MySqlSqlxDatabase::default_config().as_config())
        .add_configuration(TeloxideDispatcher::default_config().as_config())
        .add_configuration(WaBlasService::default_config().as_config())
        .add_configuration(KhanzaService::default_config().as_config())
        .add_json_file(config_file()?.is().optional())
        .add_json_file("appsetting.json".is().optional())
        .build()
        .unwrap();
    let config = Arc::from(config_builder);
    let mut service = ServiceCollection::new();
    service
        .with_mysql_sqlx_database(&config)
        .with_teloxide_dispatcher(&config)
        .with_wablas_service(&config)
        .with_khanza_service(&config)
        .with_base_task_dispatcher();

    let provider = service
        .build_provider()
        .expect("error when building provider");

    Ok(provider)
}
