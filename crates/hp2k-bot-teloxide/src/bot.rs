use chrono::{DateTime, Local};
use config::{ext::MemoryConfigurationBuilderExtensions, ConfigurationBuilder};
use std::{ops::Deref, sync::Arc};
use teloxide::prelude::*;

use di::injectable;
use options::Options;
use teloxide::Bot;

use tracing::info;

use crate::handler::{
    send_ask_otw_message, send_ask_schedule_from_previous_message, send_ask_schedule_new_message,
};

#[derive(Default, serde::Deserialize)]
pub struct TeloxideBotOption {
    pub token: String,
}

#[derive(Debug, Clone)]
pub struct TeloxideBot(pub(crate) Bot);

#[injectable]
impl TeloxideBot {
    pub fn new(option: Arc<dyn Options<TeloxideBotOption>>) -> Self {
        info!("Initializing teloxide bot");
        Self(Bot::new(&option.value().token))
    }

    pub fn default_config() -> Box<dyn config::ConfigurationRoot> {
        config::DefaultConfigurationBuilder::new()
            .add_in_memory(&[(
                "telegram:token",
                "6944121160:AAFY-b0-gXd-9MHJDTTnEA-zJDlqoQFe-Ek",
            )])
            .build()
            .unwrap()
    }
}

impl Deref for TeloxideBot {
    type Target = Bot;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TeloxideBot {
    pub async fn send_ask_next_schedule_from_prev_message(
        &self,
        chat_id: i64,
        next_date: DateTime<Local>,
    ) -> anyhow::Result<()> {
        send_ask_schedule_from_previous_message(&self.0, ChatId(chat_id), next_date).await
    }

    pub async fn send_ask_next_schedule_new_message(&self, chat_id: i64) -> anyhow::Result<()> {
        send_ask_schedule_new_message(&self.0, ChatId(chat_id)).await
    }

    pub async fn send_ask_otw_message(&self, chat_id: i64) -> anyhow::Result<()> {
        send_ask_otw_message(&self.0, ChatId(chat_id)).await
    }
}
