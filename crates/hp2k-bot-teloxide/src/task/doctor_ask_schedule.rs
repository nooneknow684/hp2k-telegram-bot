use std::sync::Arc;

use anyhow::Context;
use hp2k_bot_core::{
    Doctor, DoctorAskSchedulePayload, Schedule, StorageService, TaskPayloadParser,
    TaskPayloadRunner, TaskRegisterable,
};
use tracing::{instrument, warn};

use crate::TeloxideBot;

pub struct DoctorAskScheduleTaskRunner {
    bot: Arc<TeloxideBot>,
    db: Arc<dyn StorageService>,
}

impl TaskPayloadParser for DoctorAskScheduleTaskRunner {
    type Payload = DoctorAskSchedulePayload;
}

impl TaskRegisterable for DoctorAskScheduleTaskRunner {
    type Payload = DoctorAskSchedulePayload;
    fn from_provider(provider: &di::ServiceProvider) -> Self {
        Self {
            bot: provider.get_required(),
            db: provider.get_required(),
        }
    }
}

#[async_trait::async_trait]
impl TaskPayloadRunner for DoctorAskScheduleTaskRunner {
    #[instrument(skip_all)]
    async fn run(&self, payload: String) -> anyhow::Result<()> {
        let payload = Self::from_payload(&payload)?;

        let result = tokio::join!(
            self.db.get_doctor(&payload.code),
            self.db
                .get_last_doctor_schedule(&payload.code, chrono::Local::now().date_naive())
        );

        match result {
            (
                Ok(Doctor {
                    chat_id: Some(chat_id),
                    ..
                }),
                Ok(Schedule { schedule_at, .. }),
            ) => self
                .bot
                .send_ask_next_schedule_from_prev_message(chat_id, schedule_at)
                .await
                .context("error when sending message")?,
            (
                Ok(Doctor {
                    chat_id: Some(chat_id),
                    ..
                }),
                Err(hp2k_bot_core::StorageServiceError::SchedulePrevWeekNotFound { .. }),
            ) => self
                .bot
                .send_ask_next_schedule_new_message(chat_id)
                .await
                .context("error when sending message")?,
            (
                Ok(Doctor {
                    code,
                    chat_id: None,
                    ..
                }),
                _,
            ) => {
                warn!("doctor with code {} doesn't have chat_id", code);
                return Ok(());
            }
            (Err(hp2k_bot_core::StorageServiceError::DoctorCodeNotFound { doctor_code }), _) => {
                warn!(
                    "doctor with code {} is not exists in databaase",
                    doctor_code
                );
                return Ok(());
            }
            (Err(err), _) => {
                return Err(err).context("error when getting doctor from database");
            }
            (_, Err(err)) => {
                return Err(err).context("error when getting last schedule from database");
            }
        };

        Ok(())
    }
}
