use std::sync::Arc;

use hp2k_bot_core::{
    Doctor, DoctorAskOtwPayload, StorageService, StorageServiceError, TaskPayloadParser,
    TaskPayloadRunner, TaskRegisterable,
};
use tracing::{instrument, warn};

use crate::TeloxideBot;

pub struct DoctorAskOtwTaskRunner {
    bot: Arc<TeloxideBot>,
    db: Arc<dyn StorageService>,
}

impl TaskPayloadParser for DoctorAskOtwTaskRunner {
    type Payload = DoctorAskOtwPayload;
}

impl TaskRegisterable for DoctorAskOtwTaskRunner {
    type Payload = DoctorAskOtwPayload;
    fn from_provider(provider: &di::ServiceProvider) -> Self {
        Self {
            bot: provider.get_required(),
            db: provider.get_required(),
        }
    }
}

#[async_trait::async_trait]
impl TaskPayloadRunner for DoctorAskOtwTaskRunner {
    #[instrument(skip_all)]
    async fn run(&self, payload: String) -> anyhow::Result<()> {
        let payload = Self::from_payload(&payload)?;

        let result = match self.db.get_doctor(&payload.code).await {
            Ok(Doctor {
                chat_id: Some(chat_id),
                ..
            }) => chat_id,
            Ok(Doctor { chat_id: None, .. }) => {
                warn!("doctor is not registered");
                return Ok(());
            }
            Err(StorageServiceError::DoctorCodeNotFound { doctor_code }) => {
                warn!("doctor is not found in database {doctor_code}");
                return Ok(());
            }
            Err(_) => {
                warn!("error when getting doctor from database");
                return Ok(());
            }
        };

        self.bot.send_ask_otw_message(result).await?;

        Ok(())
    }
}
