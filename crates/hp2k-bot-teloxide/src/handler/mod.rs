mod prelude {
    use std::sync::Arc;

    use hp2k_bot_core::{BaseTaskDispatcher, StorageService};
    use teloxide::dispatching::dialogue::InMemStorage;
    use teloxide::dispatching::DpHandlerDescription;

    pub(super) type ArcStorageService = Arc<dyn StorageService>;
    pub(super) type ArcDispatcherService = Arc<BaseTaskDispatcher>;
    pub(super) type MyDialogue = Dialogue<MainState, InMemStorage<MainState>>;
    pub(super) type MyResult<T> = anyhow::Result<T>;
    pub(super) type MyOutput = MyResult<()>;
    pub(super) type MyHandler = Handler<'static, DependencyMap, MyOutput, DpHandlerDescription>;

    pub(super) type MainState = super::conversation::State;
    pub(super) use super::filter::*;
    pub(super) use crate::prelude::*;
    pub(super) type StateStorage = InMemStorage<MainState>;
    pub(super) use super::{BotUser, DoctorUser};
}

use prelude::*;

pub use command::build_command;
pub use conversation::{
    send_ask_otw_message, send_ask_schedule_from_previous_message, send_ask_schedule_new_message,
    State as MainState,
};

mod command;
mod conversation;
mod filter;

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct BotUser {
    pub chat_id: i64,
    pub user_id: u64,
    pub user: UserType,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum UserType {
    Doctor(DoctorUser),
    Employee,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct DoctorUser {
    pub code: String,
    pub name: String,
    pub speciality: String,
    pub speciality_code: String,
}

pub fn build_handler() -> MyHandler {
    dptree::entry()
        .branch(command::build_command_handler())
        .branch(conversation::build_conversation_handler())
}

mod util {
    use super::prelude::*;

    pub async fn answer_error_callback(
        bot: &Bot,
        dialogue: &MyDialogue,
        cb_id: &String,
    ) -> MyResult<()> {
        bot.answer_callback_query(cb_id)
            .text("maaf ada kesalahan dari sistem kami")
            .await?;
        dialogue.exit().await?;
        Ok(())
    }
}
