use std::sync::Arc;

use hp2k_bot_core::{Doctor, StorageService};
use teloxide::{
    dispatching::DpHandlerDescription,
    dptree::{di::DependencyMap, Handler},
    types::Update,
};
use tracing::warn;

use crate::handler::UserType;

use super::{BotUser, DoctorUser};

pub trait OtwFilter {
    fn map_user(self) -> Self;
    fn otw_registered(self) -> Self;
    fn otw_unregistered(self) -> Self;
    fn otw_doctor(self) -> Self;
    fn otw_employee(self) -> Self;
}

impl OtwFilter for Handler<'static, DependencyMap, super::MyOutput, DpHandlerDescription> {
    fn map_user(self) -> Self {
        self.map_async(|upd: Update, db: Arc<dyn StorageService>| async move {
            let user_id = upd.user()?.id;

            match db.get_doctor_by_user_id(user_id.0).await {
                Ok(Doctor {
                    code,
                    name,
                    speciality,
                    speciality_code,
                    user_id: Some(user_id),
                    chat_id: Some(chat_id),
                }) => Some(BotUser {
                    user_id,
                    chat_id,
                    user: UserType::Doctor(DoctorUser {
                        code,
                        name,
                        speciality_code,
                        speciality,
                    }),
                }),
                Ok(_) => {
                    warn!("Doctor doesn't get mapped properly");
                    None
                }
                Err(err) => {
                    warn!(%err,"Error when getting doctor from database");
                    None
                }
            }
        })
    }
    fn otw_registered(self) -> Self {
        self.filter(|bot_user: Option<BotUser>| bot_user.is_some())
    }
    fn otw_unregistered(self) -> Self {
        self.filter(|bot_user: Option<BotUser>| bot_user.is_none())
    }
    fn otw_doctor(self) -> Self {
        self.filter(|bot_user: Option<BotUser>| {
            matches!(bot_user.map(|user| user.user), Some(UserType::Doctor(_)))
        })
        .map(|user: Option<BotUser>| match user.map(|user| user.user) {
            Some(UserType::Doctor(doctor)) => doctor,
            _ => panic!("doctor is not found"),
        })
    }
    fn otw_employee(self) -> Self {
        self.filter(|bot_user: Option<BotUser>| {
            matches!(bot_user.map(|user| user.user), Some(UserType::Employee))
        })
    }
}
