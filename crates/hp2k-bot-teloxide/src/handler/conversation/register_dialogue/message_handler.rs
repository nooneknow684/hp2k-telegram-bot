use super::{prelude::*, util::build_doctor_kb_markup, State};
impl State {
    pub async fn ask_doctor_message_handler(
        bot: Bot,
        message: Message,
        dialogue: MyDialogue,
        db: ArcStorageService,
        (BaseQuestion(msg_id), filter_name, ..): (BaseQuestion, String, i64),
    ) -> MyOutput {
        bot.delete_message(message.chat.id, message.id).await?;
        let msg = match message.text() {
            Some(msg) if filter_name != msg => msg,
            Some(_) | None => {
                return Ok(());
            }
        };
        bot.edit_message_text(
            message.chat.id,
            msg_id,
            format!("Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : {msg} | 1 "),
        )
        .reply_markup(build_doctor_kb_markup(msg, 1, &db).await?)
        .await?;
        dialogue
            .update(State::doctor_registration(msg_id, msg, 1))
            .await?;
        Ok(())
    }
}
