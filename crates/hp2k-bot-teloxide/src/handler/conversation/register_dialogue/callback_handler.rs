use anyhow::anyhow;
use hp2k_bot_core::Doctor;
use teloxide::{
    dispatching::dialogue::GetChatId,
    types::{InlineKeyboardButton, InlineKeyboardMarkup},
};

pub use super::{prelude::*, State};
use super::{
    state::{
        REGISTER_DOCTOR_CHOOSE_CANCEL, REGISTER_DOCTOR_CHOOSE_CLEAR, REGISTER_DOCTOR_CHOOSE_NEXT,
        REGISTER_DOCTOR_CHOOSE_PREV, REGISTER_DOCTOR_CONFIRM_NO, REGISTER_DOCTOR_CONFIRM_YES,
        REGISTER_START_DOCTOR, REGISTER_START_EMPLOYEE,
    },
    util::{build_doctor_kb_markup, parse_register_doctor_code},
};
impl State {
    pub async fn start_callback_handler(
        bot: Bot,
        dialogue: MyDialogue,
        callback: CallbackQuery,
        db: ArcStorageService,
        BaseQuestion(message_id): BaseQuestion,
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan dalam sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("chat_id is not exists"));
            }
        };

        match callback.data.as_deref() {
            Some(REGISTER_START_DOCTOR) => {
                bot.edit_message_text(
                    chat_id,
                    message_id,
                    "Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : \"\" | 1 ",
                )
                .reply_markup(build_doctor_kb_markup("", 1, &db).await?)
                .await?;

                dialogue
                    .update(State::doctor_registration(message_id, "", 1))
                    .await?;
            }
            Some(REGISTER_START_EMPLOYEE) => {
                bot.edit_message_text(
                    chat_id,
                    message_id,
                    "Maaf fitur ini belum tersedia, mohon tunggu kehadirannya ya",
                )
                .await?;
                dialogue.exit().await?;
            }
            Some(_) => {
                bot.answer_callback_query(callback.id).await?;
            }
            None => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan dalam sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("callback data is none"));
            }
        };

        Ok(())
    }

    pub async fn ask_doctor_callback_handler(
        bot: Bot,
        callback: CallbackQuery,
        dialogue: MyDialogue,
        db: ArcStorageService,
        (BaseQuestion(msg_id), name_filter, page): (BaseQuestion, String, i64),
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan di sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("chat id is empty"));
            }
        };

        let data = match callback.data.as_deref() {
            Some(REGISTER_DOCTOR_CHOOSE_NEXT) => {
                let msg = format!("Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : \"{name_filter}\" | {} ",page+1);
                bot.edit_message_text(chat_id, msg_id, msg)
                    .reply_markup(build_doctor_kb_markup(&name_filter, page + 1, &db).await?)
                    .await?;
                dialogue
                    .update(State::doctor_registration(msg_id, name_filter, page + 1))
                    .await?;
                return Ok(());
            }
            Some(REGISTER_DOCTOR_CHOOSE_PREV) => {
                let msg = format!("Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : \"{name_filter}\" | {}",page-1);
                bot.edit_message_text(chat_id, msg_id, msg)
                    .reply_markup(build_doctor_kb_markup(&name_filter, page - 1, &db).await?)
                    .await?;
                dialogue
                    .update(State::doctor_registration(msg_id, name_filter, page - 1))
                    .await?;
                return Ok(());
            }
            Some(REGISTER_DOCTOR_CHOOSE_CLEAR) => {
                bot.edit_message_text(
                    chat_id,
                    msg_id,
                    "Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : \"\" | 1 ",
                )
                .reply_markup(build_doctor_kb_markup("", 1, &db).await?)
                .await?;
                dialogue
                    .update(State::doctor_registration(msg_id, "", 1))
                    .await?;
                return Ok(());
            }
            Some(REGISTER_DOCTOR_CHOOSE_CANCEL) => {
                bot.edit_message_text(chat_id, msg_id, "Yah, gak jadi kenalan dong")
                    .await?;
                dialogue.exit().await?;
                return Ok(());
            }
            Some(rest) => match parse_register_doctor_code(rest) {
                Ok(doctor_code) => doctor_code,
                Err(_) => {
                    bot.answer_callback_query(callback.id).await?;
                    return Ok(());
                }
            },
            None => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan di sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("callback data is empty"));
            }
        };

        let doctor = db.get_doctor(&data).await?;

        let msg = format!(
            "Ok dok\nAnda akan dikenali sebagai {} ({}) dari poliklinik {} ya dok",
            doctor.name, doctor.code, doctor.speciality
        );
        bot.edit_message_text(chat_id, msg_id, msg)
            .reply_markup(InlineKeyboardMarkup::new([[
                InlineKeyboardButton::callback("Ok", REGISTER_DOCTOR_CONFIRM_YES),
                InlineKeyboardButton::callback("Pilih Lagi", REGISTER_DOCTOR_CONFIRM_NO),
            ]]))
            .await?;

        dialogue
            .update(State::doctor_registration_confirm(msg_id, doctor))
            .await?;

        Ok(())
    }

    pub async fn ask_doctor_confirmation_handler(
        bot: Bot,
        callback: CallbackQuery,
        dialogue: MyDialogue,
        db: ArcStorageService,
        (BaseQuestion(msg_id), doctor): (BaseQuestion, Doctor),
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan dalam sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("chat_id is not exists"));
            }
        };

        match callback.data.as_deref() {
            Some(REGISTER_DOCTOR_CONFIRM_YES) => {
                let msg = format!("Salam kenal {} dari {}", doctor.name, doctor.speciality);
                db.map_doctor(&doctor.code, callback.from.id.0, chat_id.0)
                    .await?;
                bot.edit_message_text(chat_id, msg_id, msg).await?;
                dialogue.exit().await?;
            }
            Some(REGISTER_DOCTOR_CONFIRM_NO) => {
                bot.edit_message_text(
                    chat_id,
                    msg_id,
                    "Ok dok, kalo boleh tau. nama dokter siapa ya?\nFilter : \"\" | 1 ",
                )
                .reply_markup(build_doctor_kb_markup("", 1, &db).await?)
                .await?;

                dialogue
                    .update(State::doctor_registration(msg_id, "", 1))
                    .await?;
            }
            _ => {
                bot.answer_callback_query(callback.id)
                    .text("Maaf, ada kesalahan dalam sistem kami")
                    .await?;
                dialogue.exit().await?;
                return Err(anyhow!("callback data is empty or unknown"));
            }
        };

        Ok(())
    }
}
