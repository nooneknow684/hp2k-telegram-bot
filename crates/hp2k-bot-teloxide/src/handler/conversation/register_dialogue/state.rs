use hp2k_bot_core::Doctor;

use super::prelude::*;
#[derive(Debug, Clone)]
pub enum State {
    ChoosingType {
        base_question: BaseQuestion,
    },
    DoctorRegistration {
        base_question: BaseQuestion,
        name_filter: String,
        page: i64,
    },

    DoctorRegistrationConfirm {
        base_question: BaseQuestion,
        doctor: Doctor,
    },

    EmployeRegistration {
        base_question: BaseQuestion,
        name_filter: String,
        page: u32,
    },
}

impl State {
    pub(super) fn choosing_type(base_question: MessageId) -> Self {
        Self::ChoosingType {
            base_question: base_question.into(),
        }
    }
    pub(super) fn doctor_registration(
        base_question: MessageId,
        name_filter: impl Into<String>,
        page: i64,
    ) -> Self {
        Self::DoctorRegistration {
            base_question: base_question.into(),
            name_filter: name_filter.into(),
            page,
        }
    }
    pub(super) fn doctor_registration_confirm(base_question: MessageId, doctor: Doctor) -> Self {
        Self::DoctorRegistrationConfirm {
            base_question: BaseQuestion(base_question),
            doctor,
        }
    }
}

pub const REGISTER_START_DOCTOR: &str = "REGISTER:START:DOCTOR";
pub const REGISTER_START_EMPLOYEE: &str = "REGISTER:START:EMPLOYEE";
pub const REGISTER_DOCTOR_CHOOSE_PREFIX: &str = "REGISTER:DOCTOR:CHOOSE";
pub const REGISTER_DOCTOR_CHOOSE_NEXT: &str = "REGISTER:DOCTOR:CHOOSE_NEXT";
pub const REGISTER_DOCTOR_CHOOSE_PREV: &str = "REGISTER:DOCTOR:CHOOSE_PREV";
pub const REGISTER_DOCTOR_CHOOSE_CANCEL: &str = "REGISTER:DOCTOR:CHOOSE_CANCEL";
pub const REGISTER_DOCTOR_CHOOSE_CLEAR: &str = "REGISTER:DOCTOR:CHOOSE_CLEAR";
pub const REGISTER_DOCTOR_CONFIRM_YES: &str = "REGISTER:DOCTOR:CONFIRM:YES";
pub const REGISTER_DOCTOR_CONFIRM_NO: &str = "REGISTER:DOCTOR:CONFIRM:NO";
