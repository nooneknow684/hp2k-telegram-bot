use anyhow::anyhow;
use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use super::{
    prelude::*,
    state::{
        REGISTER_DOCTOR_CHOOSE_CANCEL, REGISTER_DOCTOR_CHOOSE_CLEAR, REGISTER_DOCTOR_CHOOSE_NEXT,
        REGISTER_DOCTOR_CHOOSE_PREFIX, REGISTER_DOCTOR_CHOOSE_PREV,
    },
};
pub async fn build_doctor_kb_markup(
    name_filter: &str,
    page: i64,
    db: &ArcStorageService,
) -> MyResult<InlineKeyboardMarkup> {
    let doctors = db.get_doctors(name_filter, page).await?;
    let doctors_button: Vec<_> = doctors
        .data
        .iter()
        .map(|doctor| {
            [InlineKeyboardButton::callback(
                &doctor.name,
                register_doctor_code(&doctor.code),
            )]
        })
        .collect();
    let mut keyboard = InlineKeyboardMarkup::new(doctors_button);

    keyboard = keyboard.append_row({
        let mut vec = Vec::new();

        if page > 1 {
            vec.push(InlineKeyboardButton::callback(
                "<<",
                REGISTER_DOCTOR_CHOOSE_PREV,
            ));
        }

        vec.push(InlineKeyboardButton::callback(
            "Batal",
            REGISTER_DOCTOR_CHOOSE_CANCEL,
        ));

        if ((doctors.count as f32) / 10f32).ceil() >= (page as f32) {
            vec.push(InlineKeyboardButton::callback(
                ">>",
                REGISTER_DOCTOR_CHOOSE_NEXT,
            ));
        }
        vec
    });

    if !name_filter.is_empty() {
        keyboard = keyboard.append_row([InlineKeyboardButton::callback(
            "Bersihkan Filter",
            REGISTER_DOCTOR_CHOOSE_CLEAR,
        )])
    }

    Ok(keyboard)
}

fn register_doctor_code(doctor_code: &str) -> String {
    format!("{REGISTER_DOCTOR_CHOOSE_PREFIX}:{doctor_code}")
}

pub fn parse_register_doctor_code(cb_data: &str) -> MyResult<String> {
    match cb_data.strip_prefix(&format!("{REGISTER_DOCTOR_CHOOSE_PREFIX}:")) {
        Some(code) => Ok(String::from(code)),
        None => Err(anyhow!("Wrong prefix")),
    }
}
