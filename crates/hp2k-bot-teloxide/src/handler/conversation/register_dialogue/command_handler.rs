use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use super::{
    prelude::*,
    state::{REGISTER_START_DOCTOR, REGISTER_START_EMPLOYEE},
    State,
};
impl State {
    pub async fn start_handler(bot: Bot, dialogue: MyDialogue, message: Message) -> MyOutput {
        let base_question = bot
            .send_message(
                message.chat.id,
                "Halo kita kenalan dulu ya\nJabatan anda apa ya?",
            )
            .reply_markup(InlineKeyboardMarkup::new([[
                InlineKeyboardButton::callback("Dokter", REGISTER_START_DOCTOR),
                InlineKeyboardButton::callback("Pegawai", REGISTER_START_EMPLOYEE),
            ]]))
            .await?;

        dialogue
            .update(State::choosing_type(base_question.id))
            .await?;

        Ok(())
    }
}
