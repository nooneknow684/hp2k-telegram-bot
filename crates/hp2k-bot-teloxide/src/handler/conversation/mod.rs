mod forget_dialogue;
mod otw_dialogue;
mod register_dialogue;
mod schedule_dialogue;

use std::ops::Deref;

use chrono::{DateTime, Local};
pub use forget_dialogue::State as ForgetState;
pub use otw_dialogue::State as AskOtwState;
pub use register_dialogue::State as RegisterState;
pub use schedule_dialogue::State as ScheduleState;

use prelude::*;
use teloxide::{dispatching::dialogue::GetChatId, types::MessageId};
use tracing::error;

pub mod prelude {
    use std::ops::Deref;

    pub(super) use super::super::prelude::*;
    pub(super) use super::BaseQuestion;
    pub(super) use teloxide::types::MessageId;

    #[derive(Debug, Clone, derive_more::From)]
    pub struct ErrorReply(pub MessageId);

    impl Deref for ErrorReply {
        type Target = MessageId;
        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }
}

#[derive(Debug, Clone, derive_more::From)]
pub struct BaseQuestion(pub MessageId);

impl Deref for BaseQuestion {
    type Target = MessageId;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug, Default, Clone)]
pub enum State {
    #[default]
    Idle,
    Register(RegisterState),
    AskOtw(AskOtwState),
    Forget(ForgetState),
    Schedule(ScheduleState),
}

impl From<RegisterState> for State {
    fn from(value: RegisterState) -> Self {
        Self::Register(value)
    }
}

impl From<AskOtwState> for State {
    fn from(value: AskOtwState) -> Self {
        Self::AskOtw(value)
    }
}

impl From<ForgetState> for State {
    fn from(value: ForgetState) -> Self {
        Self::Forget(value)
    }
}

impl From<ScheduleState> for State {
    fn from(value: ScheduleState) -> Self {
        Self::Schedule(value)
    }
}

pub async fn send_ask_schedule_from_previous_message(
    bot: &Bot,
    chat_id: ChatId,
    next_schedule: DateTime<Local>,
) -> Result<(), anyhow::Error> {
    ScheduleState::send_ask_schedule_from_previous_message(bot, chat_id, next_schedule).await
}

pub async fn send_ask_schedule_new_message(bot: &Bot, chat_id: ChatId) -> anyhow::Result<()> {
    ScheduleState::send_ask_schedule_new_message(bot, chat_id).await
}

pub async fn send_ask_otw_message(bot: &Bot, chat_id: ChatId) -> anyhow::Result<()> {
    AskOtwState::send_ask_otw_message(bot, chat_id).await
}

pub fn build_conversation_handler() -> MyHandler {
    dptree::entry()
        .branch(build_callback_handler())
        .branch(build_message_handler())
}

fn build_callback_handler() -> MyHandler {
    Update::filter_callback_query()
        .enter_dialogue::<CallbackQuery, StateStorage, State>()
        .branch(
            dptree::case![State::Register(register_state)]
                .branch(
                    dptree::case![RegisterState::ChoosingType { base_question }]
                        .endpoint(RegisterState::start_callback_handler),
                )
                .branch(
                    dptree::case![RegisterState::DoctorRegistration {
                        base_question,
                        name_filter,
                        page
                    }]
                    .endpoint(RegisterState::ask_doctor_callback_handler),
                )
                .branch(
                    dptree::case![RegisterState::DoctorRegistrationConfirm {
                        base_question,
                        doctor
                    }]
                    .endpoint(RegisterState::ask_doctor_confirmation_handler),
                ),
        )
        .branch(
            dptree::case![State::AskOtw(otw_state)]
                .branch(
                    dptree::case![AskOtwState::AskWhenOtw { base_question }]
                        .endpoint(AskOtwState::when_otw_callback_handler),
                )
                .branch(
                    dptree::case![AskOtwState::ReaskOtw {
                        base_question,
                        otw_id
                    }]
                    .map_user()
                    .otw_doctor()
                    .endpoint(AskOtwState::reask_otw_callback_handler),
                )
                .branch(
                    dptree::case![AskOtwState::ConfirmOtw {
                        base_question,
                        otw_at,
                        eta_at
                    }]
                    .map_user()
                    .otw_doctor()
                    .endpoint(AskOtwState::confirm_otw_callback_handler),
                ),
        )
        .branch(
            dptree::case![State::Schedule(schedule_state)]
                .branch(
                    dptree::case![ScheduleState::AskDay { base_question }]
                        .map_user()
                        .otw_doctor()
                        .endpoint(ScheduleState::choose_day_callback_handler),
                )
                .branch(
                    dptree::case![ScheduleState::Confirm {
                        base_question,
                        selected_date,
                        selected_hour
                    }]
                    .map_user()
                    .otw_doctor()
                    .endpoint(ScheduleState::confirm_callback_handler),
                ),
        )
        .branch(
            dptree::case![State::Forget(forget_state)].branch(
                dptree::case![ForgetState::Confirm { base_question }]
                    .map_user()
                    .otw_registered()
                    .endpoint(ForgetState::confirm_callback_handler),
            ),
        )
        .branch(
            dptree::filter(|callback: CallbackQuery| match callback.data.as_deref() {
                Some(schedule_dialogue::AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES)
                | Some(schedule_dialogue::AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE) => true,
                Some(any)
                    if schedule_dialogue::AUTO_ASK_SCHEDULE_CHOOSE_DAY
                        .iter()
                        .any(|i| *i == any) =>
                {
                    true
                }
                _ => false,
            })
            .map_user()
            .otw_doctor()
            .endpoint(ScheduleState::auto_start_callback_handler),
        )
        .branch(
            dptree::filter(|callback: CallbackQuery| {
                matches!(
                    callback.data.as_deref(),
                    Some(otw_dialogue::ASK_OTW_AUTO_CANCEL)
                        | Some(otw_dialogue::ASK_OTW_AUTO_WHEN_LATER)
                        | Some(otw_dialogue::ASK_OTW_AUTO_WHEN_NOW)
                )
            })
            .endpoint(AskOtwState::auto_callback_handler),
        )
        .endpoint(unknown_callback_handler)
}

async fn unknown_callback_handler(bot: Bot, callback: CallbackQuery) -> anyhow::Result<()> {
    error!("unknown callback detected");
    if let (Some(chat_id), Some(message)) = (callback.chat_id(), callback.message) {
        bot.delete_message(chat_id, message.id).await?;
    }
    bot.answer_callback_query(callback.id)
        .text("Maaf, mohon maaf ada kesalahan di sistem kami")
        .await?;
    Ok(())
}

fn build_message_handler() -> MyHandler {
    Update::filter_message()
        .enter_dialogue::<Message, StateStorage, State>()
        .branch(
            dptree::case![State::Register(register_state)].branch(
                dptree::case![RegisterState::DoctorRegistration {
                    base_question,
                    name_filter,
                    page
                }]
                .endpoint(RegisterState::ask_doctor_message_handler),
            ),
        )
        .branch(
            dptree::case![State::AskOtw(otw_state)]
                .branch(
                    dptree::case![AskOtwState::AskOtwAt {
                        base_question,
                        error_reply
                    }]
                    .endpoint(AskOtwState::ask_otw_at_handler),
                )
                .branch(
                    dptree::case![AskOtwState::AskEtaAt {
                        base_question,
                        error_reply,
                        otw_at
                    }]
                    .endpoint(AskOtwState::ask_eta_handler),
                ),
        )
        .branch(
            dptree::case![State::Schedule(schedule_state)].branch(
                dptree::case![ScheduleState::AskHour {
                    base_question,
                    error_reply,
                    selected_date
                }]
                .map_user()
                .otw_doctor()
                .endpoint(ScheduleState::ask_hour_handler),
            ),
        )
}
