use chrono::{Datelike, NaiveDate, NaiveTime};
use teloxide::{ApiError, RequestError};

use crate::handler::conversation::schedule_dialogue::{
    state::DAY_LIST, util::build_confirm_keyboard,
};

use super::{prelude::*, State};
impl State {
    pub async fn ask_hour_handler(
        bot: Bot,
        message: Message,
        dialogue: MyDialogue,
        (base_question, error_reply, date): (BaseQuestion, Option<ErrorReply>, NaiveDate),
    ) -> anyhow::Result<()> {
        let time = match parse_hour_from_message(&message) {
            Ok(time) => time,
            Err(err) => {
                let res = tokio::try_join!(
                    async {
                        bot.delete_message(message.chat.id, message.id).await?;
                        Ok(())
                    },
                    async {
                        match error_reply {
                            Some(reply) => {
                                match bot.edit_message_text(message.chat.id, reply.0, err).await {
                                    Ok(msg) => Ok(msg.id),
                                    Err(RequestError::Api(ApiError::MessageNotModified)) => {
                                        Ok(reply.0)
                                    }
                                    Err(rest) => Err(rest),
                                }
                            }
                            None => bot
                                .send_message(message.chat.id, err)
                                .await
                                .map(|item| item.id),
                        }
                    }
                );
                let (_, err) = res?;
                dialogue
                    .update(Self::ask_hour(base_question.0, Some(err), date))
                    .await?;
                return Ok(());
            }
        };

        tokio::try_join!(
            async {
                bot.delete_message(message.chat.id, message.id).await?;
                Ok(())
            },
            async {
                if let Some(errpy) = error_reply {
                    bot.delete_message(message.chat.id, errpy.0).await?;
                }
                Ok(()) as MyOutput
            },
            async {
                let date_time = date
                    .and_time(time)
                    .and_local_timezone(Local)
                    .latest()
                    .unwrap();

                let msg = format!(
                    "Ok dok, dokter berangkat lagi hari {}, tanggal {}, jam {} ya dok",
                    DAY_LIST[date_time.weekday().number_from_sunday() as usize],
                    date_time.format("%d-%m-%Y"),
                    date_time.format("%H:%M"),
                );
                bot.edit_message_text(message.chat.id, base_question.0, msg)
                    .reply_markup(build_confirm_keyboard())
                    .await?;
                Ok(())
            }
        )?;

        dialogue
            .update(Self::confirm(base_question.0, date, time))
            .await?;
        Ok(())
    }
}

fn parse_hour_from_message(message: &Message) -> Result<NaiveTime, &str> {
    let text = message
        .text()
        .ok_or("maaf dok, jadwal otw nya hanya dalam bentuk text ya")?;
    text.parse().map_err(|_| "maaf dok, format jamnya salah ya")
}
