use anyhow::Context;
use chrono::{Datelike, Days, Duration, NaiveDate, NaiveTime};
use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use crate::handler::util::answer_error_callback;

use super::{
    prelude::*,
    state::{
        ASK_SCHEDULE_CHOOSE_DAY, ASK_SCHEDULE_CONFIRM_CHOOSE_DATE,
        ASK_SCHEDULE_CONFIRM_CHOOSE_HOUR, ASK_SCHEDULE_CONFIRM_YES, AUTO_ASK_SCHEDULE_CHOOSE_DAY,
        AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE, AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES, DAY_LIST,
    },
    util::build_confirm_keyboard,
    State,
};

impl State {
    pub async fn auto_start_callback_handler(
        bot: Bot,
        callback: CallbackQuery,
        doctor: DoctorUser,
        dialogue: MyDialogue,
        db: ArcStorageService,
    ) -> anyhow::Result<()> {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => return answer_error_callback(&bot, &dialogue, &callback.id).await,
        };

        let callback_message_id = match callback.message {
            Some(message) => message.id,
            None => return answer_error_callback(&bot, &dialogue, &callback.id).await,
        };

        let now = Local::now();

        match callback.data.as_deref() {
            Some(AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES) => {
                let date_now = now.date_naive();
                let next_schedule = db.get_last_doctor_schedule(&doctor.code, date_now).await?;
                let next_date = next_schedule
                    .schedule_at
                    .checked_add_days(Days::new(7))
                    .context("error when adding 1 week to closest schedule day")?;
                db.set_schedule(&doctor.code, date_now, next_date).await?;
                let message = format!(
                    "Ok dok, dokter rencana berangkat lagi hari {} tanggal {} jam {} ya dok]",
                    DAY_LIST[next_schedule.schedule_at.weekday().num_days_from_sunday() as usize],
                    next_schedule.schedule_at.format("%d-%m-%Y"),
                    next_schedule.schedule_at.format("%H:%M"),
                );
                bot.edit_message_text(chat_id, callback_message_id, message)
                    .reply_markup(build_confirm_keyboard())
                    .await?;
                dialogue.exit().await?;
            }
            Some(AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE) => {
                let mut kb = Vec::new();
                let day_index = now.weekday().num_days_from_sunday();
                for i in day_index + 1..day_index + 7 + 1 {
                    kb.push([InlineKeyboardButton::callback(
                        DAY_LIST[(i % 7) as usize],
                        ASK_SCHEDULE_CHOOSE_DAY[(i % 7) as usize],
                    )])
                }
                bot.edit_message_text(
                    chat_id,
                    callback_message_id,
                    "Ok dok, berangkat lagi hari apa ya dok?",
                )
                .reply_markup(InlineKeyboardMarkup::new(kb))
                .await?;
                dialogue.update(Self::ask_day(callback_message_id)).await?;
            }
            Some(item) if AUTO_ASK_SCHEDULE_CHOOSE_DAY.iter().any(|my| *my == item) => {
                let day_index = AUTO_ASK_SCHEDULE_CHOOSE_DAY
                    .iter()
                    .position(|my| *my == item)
                    .context("item position is not found, this should not been happening")?
                    as u32;
                let add_days = (7 + day_index - now.weekday().num_days_from_sunday()) % 7;
                let selected_day = now.checked_add_days(Days::new(add_days as u64)).context(
                    "error when adding into selected days, this should not been happening",
                )?;

                let message = format!(
                    "Ok dok, dokter berangkat lagi hari {} tanggal {} ya dok. Untuk jam nya, jam berapa ya dok?",
                    DAY_LIST[selected_day.weekday().num_days_from_sunday() as usize],
                    selected_day.format("%d-%m-%Y")
                );
                bot.edit_message_text(chat_id, callback_message_id, message)
                    .await?;

                dialogue
                    .update(Self::ask_hour(
                        callback_message_id,
                        None,
                        selected_day.date_naive(),
                    ))
                    .await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };

        Ok(())
    }

    pub(crate) async fn choose_day_callback_handler(
        bot: Bot,
        callback: CallbackQuery,
        dialogue: MyDialogue,
        base_question: BaseQuestion,
    ) -> anyhow::Result<()> {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                error!("chat_id is empty for some reason");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };

        match callback.data.as_deref() {
            Some(item) if ASK_SCHEDULE_CHOOSE_DAY.iter().any(|m| *m == item) => {
                let day = ASK_SCHEDULE_CHOOSE_DAY
                    .iter()
                    .position(|my| *my == item)
                    .context("item position is not found, this should not been happening")?
                    as u32;
                let now = Local::now();
                let add_days = (7 + day - now.weekday().num_days_from_sunday()) % 7;
                let selected_day = now
                    .checked_add_days(Days::new(add_days as u64))
                    .context("failed when adding date to next selected day")?;

                let message = format!(
                    "Ok dok, dokter berangkat lagi hari {} tanggal {}. Berangkat jam berapa dok?",
                    DAY_LIST[add_days as usize],
                    selected_day.format("%d-%m-%Y")
                );
                bot.edit_message_text(chat_id, base_question.0, message)
                    .await?;

                dialogue
                    .update(Self::ask_hour(
                        base_question.0,
                        None,
                        selected_day.date_naive(),
                    ))
                    .await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }

        Ok(())
    }

    pub(crate) async fn confirm_callback_handler(
        bot: Bot,
        callback: CallbackQuery,
        dialogue: MyDialogue,
        db: ArcStorageService,
        doctor: DoctorUser,
        dispatcher: ArcDispatcherService,
        (base_question, selected_date, selected_hour): (BaseQuestion, NaiveDate, NaiveTime),
    ) -> anyhow::Result<()> {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => return answer_error_callback(&bot, &dialogue, &callback.id).await,
        };

        let schedule_at = selected_date
            .and_time(selected_hour)
            .and_local_timezone(Local)
            .latest()
            .context("error when parsing schedule to local time")?;

        match callback.data.as_deref() {
            Some(ASK_SCHEDULE_CONFIRM_YES) => {
                let message = format!(
                    "Ok dok, dokter berangkat lagi hari {} tanggal {} jam {} ya dok. Untuk konfirmasi otwnya akan ditanyakan nanti ya dok",
                    DAY_LIST[schedule_at.weekday().num_days_from_sunday() as usize],
                    schedule_at.format("%d-%m-%Y"),
                    schedule_at.format("%H:%M"),
                );

                bot.edit_message_text(chat_id, base_question.0, message)
                    .await?;
                db.set_schedule(&doctor.code, schedule_at.date_naive(), schedule_at)
                    .await
                    .context("error when setting schedule")?;

                dispatcher
                    .doctor_ask_otw()
                    .set_doctor(&doctor.code)
                    .execute_at(
                        schedule_at
                            .checked_sub_signed(Duration::hours(2))
                            .context("error when substracting schedule at hour")?,
                    )
                    .dispatch()
                    .await?;
                dialogue.exit().await?;
            }
            Some(ASK_SCHEDULE_CONFIRM_CHOOSE_DATE) => {
                let msg = "Ok dok, dokter datang hari apa lagi ya dok?";
                let mut kb = Vec::new();

                let current_day_index = Local::now().weekday().num_days_from_sunday();
                for i in current_day_index + 1..current_day_index + 1 + 7 {
                    kb.push([InlineKeyboardButton::callback(
                        DAY_LIST[(i % 7) as usize],
                        ASK_SCHEDULE_CHOOSE_DAY[(i % 7) as usize],
                    )]);
                }

                bot.edit_message_text(chat_id, base_question.0, msg)
                    .reply_markup(InlineKeyboardMarkup::new(kb))
                    .await?;

                dialogue.update(Self::ask_day(base_question.0)).await?;
            }
            Some(ASK_SCHEDULE_CONFIRM_CHOOSE_HOUR) => {
                let msg = format!(
                    "Ok dok, dokter berangkat lagi hari {} tanggal {} ya. Berangkat jam berapa dok?",
                    DAY_LIST[schedule_at.weekday().num_days_from_sunday() as usize],
                    schedule_at.format("%d-%m-%Y"),
                    );
                bot.edit_message_text(chat_id, base_question.0, msg).await?;
                dialogue
                    .update(Self::ask_hour(base_question.0, None, selected_date))
                    .await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }

        Ok(())
    }
}
