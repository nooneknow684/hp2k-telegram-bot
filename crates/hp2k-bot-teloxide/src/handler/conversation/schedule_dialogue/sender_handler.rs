use chrono::Datelike;
use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

pub use super::prelude::*;
use super::{
    state::{
        AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE, AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES, DAY_LIST,
    },
    State, AUTO_ASK_SCHEDULE_CHOOSE_DAY,
};
impl State {
    pub(crate) async fn send_ask_schedule_from_previous_message(
        bot: &Bot,
        chat_id: ChatId,
        next_schedule: DateTime<Local>,
    ) -> anyhow::Result<()> {
        let message = format!(
                    "Halo dok, maaf mengganggu waktunya. Mau konfirmasi dok, dokter berangkat lagi hari {} tanggal {} jam {} ya dok", 
                    DAY_LIST[next_schedule.weekday().num_days_from_sunday() as usize],
                    next_schedule.format("%d-%m-%Y"),
                    next_schedule.format("%H:%M")
                    );
        let rm = InlineKeyboardMarkup::new([[
            InlineKeyboardButton::callback("Ya", AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES),
            InlineKeyboardButton::callback(
                "Ganti Tanggal",
                AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE,
            ),
        ]]);
        bot.send_message(chat_id, message).reply_markup(rm).await?;
        Ok(())
    }
    pub(crate) async fn send_ask_schedule_new_message(
        bot: &Bot,
        chat_id: ChatId,
    ) -> anyhow::Result<()> {
        let now = Local::now();
        let message = "Halo dok, maaf mengganggu waktunya. Dokter ke rumah sakit lagi kapan ya?";
        let now_date = now.weekday().num_days_from_sunday();
        let mut kb = Vec::new();
        for i in now_date + 1..now_date + 1 + 7 {
            kb.push([InlineKeyboardButton::callback(
                DAY_LIST[(i % 7) as usize],
                AUTO_ASK_SCHEDULE_CHOOSE_DAY[(i % 7) as usize],
            )])
        }
        let rm = InlineKeyboardMarkup::new(kb);
        bot.send_message(chat_id, message).reply_markup(rm).await?;
        Ok(())
    }
}
