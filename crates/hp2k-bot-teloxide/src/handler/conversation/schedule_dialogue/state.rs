use chrono::{NaiveDate, NaiveTime};

use super::prelude::*;

pub const DAY_LIST: [&str; 7] = [
    "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu",
];

pub const AUTO_ASK_SCHEDULE_CHOOSE_DAY: [&str; 7] = [
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:0",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:2",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:1",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:3",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:4",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:5",
    "ASK_SCHEDULE:AUTO:CHOOSE_DAY:6",
];

pub const AUTO_ASK_SCHEDULE_CONFIRM_NEXT_YES: &str = "ASK_SCHEDULE:AUTO:CONFIRM_NEXT:YES";
pub const AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_DATE: &str =
    "ASK_SCHEDULE:AUTO:CONFIRM_NEXT:CHOOSE_DAY";
// pub const AUTO_ASK_SCHEDULE_CONFIRM_NEXT_CHOOSE_HOUR: &str =
//     "ASK_SCHEDULE:AUTO:CONFIRM_NEXT:CHOOSE_HOUR";

pub const ASK_SCHEDULE_CHOOSE_DAY: [&str; 7] = [
    "ASK_SCHEDULE:CHOOSE_DAY:0",
    "ASK_SCHEDULE:CHOOSE_DAY:1",
    "ASK_SCHEDULE:CHOOSE_DAY:2",
    "ASK_SCHEDULE:CHOOSE_DAY:3",
    "ASK_SCHEDULE:CHOOSE_DAY:4",
    "ASK_SCHEDULE:CHOOSE_DAY:5",
    "ASK_SCHEDULE:CHOOSE_DAY:6",
];

pub const ASK_SCHEDULE_CONFIRM_YES: &str = "ASK_SCHEDULE:CONFIRM:OK";
pub const ASK_SCHEDULE_CONFIRM_CHOOSE_DATE: &str = "ASK_SCHEDULE:CONFIRM:CHOOSE_DAY";
pub const ASK_SCHEDULE_CONFIRM_CHOOSE_HOUR: &str = "ASK_SCHEDULE:CONFIRM:CHOOSE_HOUR";

#[derive(Debug, Clone)]
pub enum State {
    AskDay {
        base_question: BaseQuestion,
    },
    AskHour {
        base_question: BaseQuestion,
        error_reply: Option<ErrorReply>,
        selected_date: NaiveDate,
    },
    Confirm {
        base_question: BaseQuestion,
        selected_date: NaiveDate,
        selected_hour: NaiveTime,
    },
}

impl State {
    pub(super) fn ask_day(msg: MessageId) -> Self {
        Self::AskDay {
            base_question: BaseQuestion(msg),
        }
    }

    pub(super) fn ask_hour(bq: MessageId, err_ply: Option<MessageId>, date: NaiveDate) -> Self {
        Self::AskHour {
            base_question: BaseQuestion(bq),
            error_reply: err_ply.map(ErrorReply),
            selected_date: date,
        }
    }

    pub(super) fn confirm(bq: MessageId, selected_date: NaiveDate, hour: NaiveTime) -> Self {
        Self::Confirm {
            base_question: BaseQuestion(bq),
            selected_date,
            selected_hour: hour,
        }
    }
}
