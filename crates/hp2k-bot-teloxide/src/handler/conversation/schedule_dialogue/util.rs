use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use super::state::{
    ASK_SCHEDULE_CONFIRM_CHOOSE_DATE, ASK_SCHEDULE_CONFIRM_CHOOSE_HOUR, ASK_SCHEDULE_CONFIRM_YES,
};

pub fn build_confirm_keyboard() -> InlineKeyboardMarkup {
    InlineKeyboardMarkup::new([[
        InlineKeyboardButton::callback("Ok", ASK_SCHEDULE_CONFIRM_YES),
        InlineKeyboardButton::callback("Atur Hari", ASK_SCHEDULE_CONFIRM_CHOOSE_DATE),
        InlineKeyboardButton::callback("Atur Jam", ASK_SCHEDULE_CONFIRM_CHOOSE_HOUR),
    ]])
}
