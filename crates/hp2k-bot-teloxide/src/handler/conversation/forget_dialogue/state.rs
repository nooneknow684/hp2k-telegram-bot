use super::prelude::*;

#[derive(Debug, Clone)]
pub enum State {
    Confirm { base_question: BaseQuestion },
}

impl State {
    pub(super) fn confirm(base: MessageId) -> Self {
        Self::Confirm {
            base_question: BaseQuestion(base),
        }
    }
}

pub const FORGET_CONFIRM_YES: &str = "FORGET:CONFIRM:YES";
pub const FORGET_CONFIRM_NO: &str = "FORGET:CONFIRM:NO";
