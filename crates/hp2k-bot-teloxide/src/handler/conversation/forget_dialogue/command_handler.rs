use anyhow::Context;
use teloxide::{
    dispatching::dialogue::GetChatId,
    types::{InlineKeyboardButton, InlineKeyboardMarkup},
};
use tracing::error;

use crate::handler::util::answer_error_callback;

use super::{
    prelude::*,
    state::{FORGET_CONFIRM_NO, FORGET_CONFIRM_YES},
    State,
};

impl State {
    pub async fn start_handler(bot: Bot, message: Message, dialogue: MyDialogue) -> MyOutput {
        let kb = InlineKeyboardMarkup::new([[
            InlineKeyboardButton::callback("Ya", FORGET_CONFIRM_YES),
            InlineKeyboardButton::callback("Tidak", FORGET_CONFIRM_NO),
        ]]);

        let bq = bot
            .send_message(message.chat.id, "Anda yakin mau dilupakan?")
            .reply_markup(kb)
            .await?;
        dialogue.update(Self::confirm(bq.id)).await?;
        Ok(())
    }

    pub async fn confirm_callback_handler(
        bot: Bot,
        service: ArcStorageService,
        callback: CallbackQuery,
        user: Option<BotUser>,
        dialogue: MyDialogue,
        BaseQuestion(msg): BaseQuestion,
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                error!("chat id is not found in callback instance");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };
        let BotUser {
            user_id,
            chat_id: pchat_id,
            ..
        } = user.context("user is None for some reason")?;

        match callback.data.as_deref() {
            Some(FORGET_CONFIRM_YES) => {
                service.unmap_user(user_id, pchat_id).await?;
                bot.edit_message_text(
                    chat_id,
                    msg,
                    "Aku lupa akan sesuatu. Bukan sesuatu yang penting sepertinya",
                )
                .await?;
                dialogue.exit().await?;
            }
            Some(FORGET_CONFIRM_NO) => {
                bot.edit_message_text(
                    chat_id,
                    msg,
                    "Terima kasih karena masih mau kenal dengan aku",
                )
                .await?;
                dialogue.exit().await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }
        Ok(())
    }
}
