use chrono::{DateTime, Local};

pub const ASK_OTW_WHEN_NOW: &str = "ASK_OTW:OTW_WHEN:NOW";
pub const ASK_OTW_WHEN_LATER: &str = "ASK_OTW:OTW_WHEN:LATER";
pub const ASK_OTW_CONFIRM_OK: &str = "ASK_OTW:CONFIRM:OK";
pub const ASK_OTW_CONFIRM_NO: &str = "ASK_OTW:CONFIRM:NO";
pub const ASK_OTW_REASK_RESET: &str = "ASK_OTW:REASK:RESET";
pub const ASK_OTW_REASK_CANCEL: &str = "ASK_OTW:REASK:CANCEL";
pub const ASK_OTW_REASK_EXIT: &str = "ASK_OTW:REASK:EXIT";

pub const ASK_OTW_AUTO_WHEN_NOW: &str = "ASK_OTW:AUTO:OTW_WHEN:NOW";
pub const ASK_OTW_AUTO_WHEN_LATER: &str = "ASK_OTW:AUTO:OTW_WHEN:LATER";
pub const ASK_OTW_AUTO_CANCEL: &str = "ASK_OTW:AUTO:CANCEL";

pub use super::prelude::*;
#[derive(Debug, Clone)]
pub enum State {
    ReaskOtw {
        base_question: BaseQuestion,
        otw_id: u32,
    },
    AskWhenOtw {
        base_question: BaseQuestion,
    },
    AskOtwAt {
        base_question: BaseQuestion,
        error_reply: Option<ErrorReply>,
    },
    AskEtaAt {
        base_question: BaseQuestion,
        error_reply: Option<ErrorReply>,
        otw_at: DateTime<Local>,
    },
    ConfirmOtw {
        base_question: BaseQuestion,
        otw_at: DateTime<Local>,
        eta_at: u32,
    },
}

impl State {
    pub fn reask_otw(base: MessageId, otw_id: u32) -> Self {
        Self::ReaskOtw {
            base_question: BaseQuestion(base),
            otw_id,
        }
    }
    pub fn ask_when_otw(base: MessageId) -> Self {
        Self::AskWhenOtw {
            base_question: BaseQuestion(base),
        }
    }
    pub fn ask_otw_at(base: MessageId, error: Option<MessageId>) -> Self {
        Self::AskOtwAt {
            base_question: BaseQuestion(base),
            error_reply: error.map(ErrorReply),
        }
    }
    pub fn ask_eta_at(base: MessageId, error: Option<MessageId>, otw_at: DateTime<Local>) -> Self {
        Self::AskEtaAt {
            base_question: BaseQuestion(base),
            error_reply: error.map(ErrorReply),
            otw_at,
        }
    }

    pub fn confirm_otw(base: MessageId, otw_at: DateTime<Local>, eta_at: u32) -> Self {
        Self::ConfirmOtw {
            base_question: BaseQuestion(base),
            eta_at,
            otw_at,
        }
    }
}
