use anyhow::Context;
use chrono::{DateTime, Duration, Local, NaiveTime};
use teloxide::{
    types::{InlineKeyboardButton, InlineKeyboardMarkup},
    ApiError, RequestError,
};

use super::{
    prelude::*,
    state::{ASK_OTW_CONFIRM_NO, ASK_OTW_CONFIRM_OK},
    State,
};
impl State {
    pub async fn ask_otw_at_handler(
        dialogue: MyDialogue,
        bot: Bot,
        message: Message,
        (base_question, error_reply): (BaseQuestion, Option<ErrorReply>),
    ) -> MyOutput {
        let now = Local::now();
        let otw_time = match parse_otw_at_from_message(now, &message) {
            Ok(date) => date,
            Err(err) => {
                let res = tokio::try_join!(
                    async {
                        bot.delete_message(message.chat.id, message.id).await?;
                        Ok(())
                    },
                    async {
                        match error_reply {
                            Some(reply) => {
                                match bot.edit_message_text(message.chat.id, reply.0, err).await {
                                    Ok(msg) => Ok(msg.id),
                                    Err(RequestError::Api(ApiError::MessageNotModified)) => {
                                        Ok(reply.0)
                                    }
                                    Err(rest) => Err(rest),
                                }
                            }
                            None => bot
                                .send_message(message.chat.id, err)
                                .await
                                .map(|item| item.id),
                        }
                    }
                );
                let (_, err) = res?;
                dialogue
                    .update(State::ask_otw_at(base_question.0, Some(err)))
                    .await?;
                return Ok(());
            }
        };

        let date_time = now
            .date_naive()
            .and_time(otw_time)
            .and_local_timezone(Local)
            .latest()
            .context("error when setting date time")?;

        tokio::try_join!(
            async {
                bot.delete_message(message.chat.id, message.id).await?;
                Ok(())
            },
            async {
                if let Some(error_reply) = error_reply {
                    bot.delete_message(message.chat.id, error_reply.0).await?;
                }
                Ok(()) as MyOutput
            },
            async {
                bot.edit_message_text(
                    message.chat.id,
                    base_question.0,
                    "ok dok, untuk waktu tibanya berapa lama kira-kira ya dok?",
                )
                .await?;
                Ok(())
            }
        )?;

        dialogue
            .update(State::ask_eta_at(base_question.0, None, date_time))
            .await?;

        Ok(())
    }

    pub async fn ask_eta_handler(
        bot: Bot,
        message: Message,
        dialogue: MyDialogue,
        (base_question, error_reply, otw_at): (BaseQuestion, Option<ErrorReply>, DateTime<Local>),
    ) -> MyOutput {
        let eta = match parse_eta_from_message(&message) {
            Ok(eta) => eta,
            Err(err) => {
                let (_, err) = tokio::try_join!(
                    async { bot.delete_message(message.chat.id, message.id).await },
                    async {
                        match error_reply {
                            Some(reply) => {
                                match bot.edit_message_text(message.chat.id, reply.0, err).await {
                                    Ok(msg) => Ok(msg.id),
                                    Err(RequestError::Api(
                                        teloxide::ApiError::MessageNotModified,
                                    )) => Ok(reply.0),
                                    Err(rest) => Err(rest),
                                }
                            }
                            None => bot
                                .send_message(message.chat.id, err)
                                .await
                                .map(|item| item.id),
                        }
                    }
                )?;

                dialogue
                    .update(State::ask_eta_at(base_question.0, Some(err), otw_at))
                    .await?;
                return Ok(());
            }
        };

        let kb = InlineKeyboardMarkup::new([[
            InlineKeyboardButton::callback("Ok", ASK_OTW_CONFIRM_OK),
            InlineKeyboardButton::callback("Atur Ulang", ASK_OTW_CONFIRM_NO),
        ]]);

        let message_text = format!(
        "Ok dok, dokter akan berangkat jam {} dan kemungkinan tiba jam {} atau sekitar {} menit",
        otw_at.format("%H:%M"),
        otw_at
            .checked_add_signed(Duration::minutes(eta as i64))
                .context("error when adding eta to otw_at, this should not happening")?
            .format("%H:%M"),
        eta,
    );

        tokio::try_join!(
            async {
                bot.delete_message(message.chat.id, message.id).await?;
                Ok(())
            },
            async {
                if let Some(error_reply) = error_reply {
                    bot.delete_message(message.chat.id, error_reply.0).await?;
                }
                Ok(()) as MyOutput
            },
            async {
                bot.edit_message_text(message.chat.id, base_question.0, message_text)
                    .reply_markup(kb)
                    .await?;
                Ok(())
            }
        )?;

        dialogue
            .update(State::confirm_otw(base_question.0, otw_at, eta))
            .await?;

        Ok(())
    }
}

fn parse_otw_at_from_message(
    now: DateTime<Local>,
    message: &Message,
) -> Result<chrono::NaiveTime, &str> {
    let text = message
        .text()
        .ok_or("maaf dok, jadwal otw nya hanya dalam bentuk text ya")?;
    let otw_at = NaiveTime::parse_from_str(text, "%H:%M")
        .map_err(|_| "maaf dok, formatnya salah ya, format yang benar itu jj:mm")?;

    if otw_at < now.time() {
        return Err("maaf dok, tapi jamnya gak boleh sebelum dari jam sekarang");
    }

    Ok(otw_at)
}

fn parse_eta_from_message(message: &Message) -> Result<u32, &str> {
    let text: u32 = message
        .text()
        .and_then(|item| item.parse().ok())
        .ok_or("maaf dok, waktu tiba cuman bisa di isi angka ya dok")?;

    Ok(text)
}
