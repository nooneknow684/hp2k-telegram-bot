use chrono::{DateTime, Local};
use hp2k_bot_core::{Otw, StorageServiceError};
use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use crate::handler::conversation::otw_dialogue::state::{
    ASK_OTW_REASK_CANCEL, ASK_OTW_REASK_EXIT, ASK_OTW_REASK_RESET,
};

use super::{
    prelude::*,
    state::{State, ASK_OTW_WHEN_LATER, ASK_OTW_WHEN_NOW},
};
impl State {
    pub async fn start_handler(
        dialogue: MyDialogue,
        bot: Bot,
        message: Message,
        doctor: DoctorUser,
        service: ArcStorageService,
    ) -> MyOutput {
        let otw_date = Local::now();

        match service
            .get_otw_by_date_and_doctor(otw_date.date_naive(), &doctor.code)
            .await
        {
            Ok(Otw { otw_at, eta_at, .. }) if eta_at < otw_date => {
                let otw_at: DateTime<Local> = otw_at.into();
                let eta_at: DateTime<Local> = eta_at.into();

                let eta = eta_at.signed_duration_since(otw_at).num_minutes();
                tracing::debug!("{}", otw_at);

                let msg = format!(
                    "Halo dok, anda sudah mengatur jadwal otw untuk hari ini ya dan sudah lewat ya dok. Dokter otw jam {} dan estimasi waktu tiba jam {} atau sekitar {} menit ya dok.",
                    otw_at.format("%H:%M"),
                    eta_at.format("%H:%M"),
                    eta
                );

                bot.send_message(message.chat.id, msg).await?;
            }
            Ok(Otw {
                id: otw_id,
                otw_at,
                eta_at,
                ..
            }) => {
                let kb = InlineKeyboardMarkup::new([[
                    InlineKeyboardButton::callback("Atur Ulang", ASK_OTW_REASK_RESET),
                    InlineKeyboardButton::callback("Batal OTW", ASK_OTW_REASK_CANCEL),
                    InlineKeyboardButton::callback("Lihat Saja", ASK_OTW_REASK_EXIT),
                ]]);
                let otw_at: DateTime<Local> = otw_at.into();
                let eta_at: DateTime<Local> = eta_at.into();

                let eta = eta_at.signed_duration_since(otw_at).num_minutes();
                tracing::debug!("{}", otw_at);

                let msg = format!(
                            "Halo dok, anda sudah mengatur jadwal otw untuk hari ini ya. Dokter otw jam {} dan estimasi waktu tiba jam {} atau sekitar {} menit ya dok, mau diapain dok?",
                    otw_at.format("%H:%M"),
                    eta_at.format("%H:%M"),
                    eta
                );

                let bq = bot
                    .send_message(message.chat.id, msg)
                    .reply_markup(kb)
                    .await?;
                dialogue
                    .update(super::State::reask_otw(bq.id, otw_id))
                    .await?;
            }
            Err(StorageServiceError::OtwDoctorNotFouned { .. }) => {
                let kb = InlineKeyboardMarkup::new([[
                    InlineKeyboardButton::callback("Sekarang", ASK_OTW_WHEN_NOW),
                    InlineKeyboardButton::callback("Nanti", ASK_OTW_WHEN_LATER),
                ]]);

                let bq = bot
                    .send_message(message.chat.id, "Mau otw kapan dok?")
                    .reply_markup(kb)
                    .await?;
                dialogue.update(super::State::ask_when_otw(bq.id)).await?;
            }
            Err(err) => return Err(err.into()),
        }

        Ok(())
    }
}
