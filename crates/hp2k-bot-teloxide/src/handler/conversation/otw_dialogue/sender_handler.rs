use anyhow::Context;
use teloxide::types::{InlineKeyboardButton, InlineKeyboardMarkup};

use super::{
    prelude::*, State, ASK_OTW_AUTO_CANCEL, ASK_OTW_AUTO_WHEN_LATER, ASK_OTW_AUTO_WHEN_NOW,
};

impl State {
    pub async fn send_ask_otw_message(bot: &Bot, chat_id: ChatId) -> anyhow::Result<()> {
        let msg = "Halo dok, hari ini jadi berangkat gak ya dok? kalo jadi mau berangkat kapan?";
        let kb = InlineKeyboardMarkup::new([[
            InlineKeyboardButton::callback("Sekarang", ASK_OTW_AUTO_WHEN_NOW),
            InlineKeyboardButton::callback("Nanti", ASK_OTW_AUTO_WHEN_LATER),
            InlineKeyboardButton::callback("Tidak Jadi", ASK_OTW_AUTO_CANCEL),
        ]]);
        bot.send_message(chat_id, msg)
            .reply_markup(kb)
            .await
            .context("error when sending ask otw message")?;
        Ok(())
    }
}
