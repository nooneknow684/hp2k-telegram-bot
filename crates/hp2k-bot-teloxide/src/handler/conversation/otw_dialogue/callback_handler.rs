use anyhow::Context;
use chrono::{DateTime, Duration, Local};
use teloxide::{
    dispatching::dialogue::GetChatId,
    types::{InlineKeyboardButton, InlineKeyboardMarkup},
};
use tracing::{error, warn};

use crate::handler::util::answer_error_callback;

use super::{
    prelude::*,
    state::{
        ASK_OTW_CONFIRM_NO, ASK_OTW_CONFIRM_OK, ASK_OTW_REASK_CANCEL, ASK_OTW_REASK_EXIT,
        ASK_OTW_REASK_RESET, ASK_OTW_WHEN_LATER, ASK_OTW_WHEN_NOW,
    },
    State, ASK_OTW_AUTO_CANCEL, ASK_OTW_AUTO_WHEN_LATER, ASK_OTW_AUTO_WHEN_NOW,
};

impl State {
    pub async fn auto_callback_handler(
        dialogue: MyDialogue,
        bot: Bot,
        callback: CallbackQuery,
    ) -> anyhow::Result<()> {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                warn!("chat_id is none for some reason");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };
        let callback_message_id = match callback.message {
            Some(message) => message.id,
            None => {
                warn!("callback message id is none for some reason");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };

        match callback.data.as_deref() {
            Some(ASK_OTW_AUTO_WHEN_NOW) => {
                let msg = "Ok dok, estimasi tiba berapa menit ya dok?";
                dialogue
                    .update(Self::ask_eta_at(callback_message_id, None, Local::now()))
                    .await?;
                bot.edit_message_text(chat_id, callback_message_id, msg)
                    .await?;
            }
            Some(ASK_OTW_AUTO_WHEN_LATER) => {
                let msg = "Ok dok, mau berangkat jam berapa ya dok?";
                dialogue
                    .update(Self::ask_otw_at(callback_message_id, None))
                    .await?;
                bot.edit_message_text(chat_id, callback_message_id, msg)
                    .await?;
            }
            Some(ASK_OTW_AUTO_CANCEL) => {
                bot.edit_message_text(
                    chat_id,
                    callback_message_id,
                    "Ok dok, doktor batal otw ya dok",
                )
                .await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }
        Ok(())
    }
    pub async fn reask_otw_callback_handler(
        dialogue: MyDialogue,
        bot: Bot,
        callback: CallbackQuery,
        service: ArcStorageService,
        (base_question, otw_id): (BaseQuestion, u32),
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                error!("chat id is not found in callback instance");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };

        match callback.data.as_deref() {
            Some(ASK_OTW_REASK_EXIT) => {
                bot.edit_message_reply_markup(chat_id, base_question.0)
                    .await?;

                bot.send_message(chat_id, "Ok dok").await?;
                dialogue.exit().await?;
            }
            Some(ASK_OTW_REASK_CANCEL) => {
                bot.edit_message_reply_markup(chat_id, base_question.0)
                    .await?;
                bot.send_message(chat_id, "Ok dok, jadwal otw di batalin ya")
                    .await?;
                service.cancel_otw(otw_id).await?;
                dialogue.exit().await?;
            }
            Some(ASK_OTW_REASK_RESET) => {
                service.cancel_otw(otw_id).await?;
                let kb = InlineKeyboardMarkup::new([[
                    InlineKeyboardButton::callback("Sekarang", ASK_OTW_WHEN_NOW),
                    InlineKeyboardButton::callback("Nanti", ASK_OTW_WHEN_LATER),
                ]]);

                let bq = bot
                        .edit_message_text(chat_id, base_question.0, "Mau otw kapan dok?\nFormat jawabannya harus berupa teks dengan format jj:mm ya dok")
                            .reply_markup(kb)
                            .await?;
                dialogue.update(State::ask_when_otw(bq.id)).await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }

        Ok(())
    }

    pub async fn when_otw_callback_handler(
        dialogue: MyDialogue,
        bot: Bot,
        callback: CallbackQuery,
        base_question: BaseQuestion,
    ) -> MyOutput {
        let Some(chat_id) = callback.chat_id() else {
                error!("chat id is not found in callback instance");
                return answer_error_callback(&bot, &dialogue, &callback.id).await
            };

        match callback.data.as_deref() {
            Some(ASK_OTW_WHEN_NOW) => {
                bot.edit_message_text(
                    chat_id,
                    base_question.0,
                    "Ok dok, kira-kira sampai di rumah sakitnya berapa menit ya dok?",
                )
                .await?;
                dialogue
                    .update(State::ask_eta_at(base_question.0, None, Local::now()))
                    .await?;
            }
            Some(ASK_OTW_WHEN_LATER) => {
                bot.edit_message_text(
                    chat_id,
                    base_question.0,
                    "Ok dok, berangkat jam berapa ya dok?\nFormat jawabannya jj:mm ya dok",
                )
                .await?;
                dialogue
                    .update(State::ask_otw_at(base_question.0, None))
                    .await?;
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }

        Ok(())
    }

    pub async fn confirm_otw_callback_handler(
        bot: Bot,
        callback: CallbackQuery,
        dialogue: MyDialogue,
        doctor: DoctorUser,
        service: ArcStorageService,
        dispatcher: ArcDispatcherService,
        (base_question, otw_at, eta): (BaseQuestion, DateTime<Local>, u32),
    ) -> MyOutput {
        let chat_id = match callback.chat_id() {
            Some(chat_id) => chat_id,
            None => {
                error!("chat id is not found in callback instance");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        };

        match callback.data.as_deref() {
            Some(ASK_OTW_CONFIRM_OK) => {}
            Some(ASK_OTW_CONFIRM_NO) => {
                let kb = InlineKeyboardMarkup::new([[
                    InlineKeyboardButton::callback("Sekarang", ASK_OTW_WHEN_NOW),
                    InlineKeyboardButton::callback("Nanti", ASK_OTW_WHEN_LATER),
                ]]);
                bot.edit_message_text(chat_id, base_question.0, "Mau otw kapan dok?\nFormat jawabannya harus berupa teks dengan format jj:mm ya dok")
                            .reply_markup(kb)
                                .await?;
                dialogue
                    .update(State::ask_when_otw(base_question.0))
                    .await?;
                return Ok(());
            }
            Some(_) => {
                error!("callback data doesn't match the pattern");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
            None => {
                error!("callback data is empty");
                return answer_error_callback(&bot, &dialogue, &callback.id).await;
            }
        }

        let eta_at = otw_at
            .checked_add_signed(Duration::minutes(eta as i64))
            .context("error when creating eta_at")?;

        let message_text = format!(
                    "Ok dok, dokter akan berangkat jam {} dan kemungkinan tiba jam {} atau sekitar {} menit. Pasien akan di kabarkan sesuai jadwal yang diberikan ya",
                    otw_at.format("%H:%M"),
                    otw_at
                        .checked_add_signed(Duration::minutes(eta as i64))
                                    .context("error nich")?
                        .format("%H:%M"),
                    eta,
                );

        bot.edit_message_text(chat_id, base_question.0, message_text)
            .await?;

        let otw = service
            .assign_otw(
                &doctor.code,
                otw_at.date_naive(),
                otw_at.into(),
                eta_at.into(),
            )
            .await?;

        tokio::try_join!(
            dispatcher
                .doctor_will_otw()
                .set_doctor(&doctor.code, &doctor.name, &doctor.speciality)
                .set_otw(otw.id, otw.otw_date, otw.otw_at.into(), otw.eta_at.into())
                .execute_at(
                    otw.otw_at
                        .checked_sub_signed(Duration::minutes(5))
                        .unwrap()
                        .into()
                )
                .dispatch(),
            dispatcher
                .doctor_will_arrive()
                .set_doctor(&doctor.code, &doctor.name, &doctor.speciality)
                .set_otw(otw.id, otw.otw_date, otw.otw_at.into(), otw.eta_at.into())
                .execute_at(
                    otw.eta_at
                        .checked_sub_signed(Duration::minutes(5))
                        .unwrap()
                        .into()
                )
                .dispatch(),
            dispatcher
                .doctor_ask_schedule()
                .set_doctor(&doctor.code)
                .dispatch()
        )?;

        dialogue.exit().await?;

        Ok(())
    }
}
