use crate::handler::conversation::AskOtwState;

use super::prelude::*;

#[derive(Debug, Clone, BotCommands)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    #[command(rename = "otw", description = "[Hanya Dokter] Atur waktu otw")]
    Otw,
}
impl Command {
    pub fn handler() -> MyHandler {
        dptree::entry()
            .filter_command::<Command>()
            .branch(
                dptree::entry()
                    .otw_doctor()
                    .branch(dptree::case![Command::Otw].endpoint(AskOtwState::start_handler)),
            )
            .endpoint(not_doctor_handler)
    }
}

async fn not_doctor_handler(bot: Bot, message: Message) -> MyOutput {
    bot.send_message(
        message.chat.id,
        "Maaf ya, command ini hanya untuk dokter sahaja",
    )
    .await?;
    Ok(())
}
