use super::prelude::*;
use crate::handler::conversation::ForgetState;

#[derive(Debug, Clone, BotCommands)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    #[command(rename = "lupakan", description = "[Semua User] Lupakan aku")]
    Forget,
}
impl Command {
    pub fn handler() -> MyHandler {
        dptree::entry().filter_command::<Command>().branch(
            dptree::case![Self::Forget]
                .branch(
                    dptree::entry()
                        .otw_registered()
                        .endpoint(ForgetState::start_handler),
                )
                .endpoint(not_registered_handler),
        )
    }
}

async fn not_registered_handler(bot: Bot, message: Message) -> MyOutput {
    bot.send_message(message.chat.id, "Lah emang kita udah kenalan? kamu siapa?")
        .await?;
    Ok(())
}
