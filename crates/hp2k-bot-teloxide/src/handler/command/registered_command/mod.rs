mod prelude {
    pub(super) use super::super::prelude::*;
}
mod doctor_command;
mod general_command;

use prelude::*;
use teloxide::types::BotCommand;

pub fn build_registered_command_handler() -> MyHandler {
    dptree::entry()
        .branch(general_command::Command::handler())
        .branch(doctor_command::Command::handler())
}

pub fn build_command() -> Vec<BotCommand> {
    let mut vec = Vec::new();

    vec.append(&mut doctor_command::Command::bot_commands());
    vec.append(&mut general_command::Command::bot_commands());
    vec
}
