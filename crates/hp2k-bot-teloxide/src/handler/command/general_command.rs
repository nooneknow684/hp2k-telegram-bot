use super::prelude::*;

#[derive(Debug, Clone, BotCommands)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    #[command(description = "[General] Tampilkan teks bantuan")]
    Help,
    #[command(description = "[General] Command khusus")]
    Start,
    #[command(description = "[General] Reset ulang bot")]
    Exit,
}

impl Command {
    pub fn handler() -> MyHandler {
        Update::filter_message()
            .filter_command::<Command>()
            .branch(dptree::case![Self::Exit].endpoint(Self::exit_handler))
    }
    async fn exit_handler(dialogue: MyDialogue) -> MyOutput {
        dialogue.exit().await?;
        Ok(())
    }
}
