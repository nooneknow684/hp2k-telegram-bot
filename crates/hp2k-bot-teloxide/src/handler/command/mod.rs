mod prelude {
    pub(super) use crate::handler::prelude::*;
    pub(super) use teloxide::utils::command::BotCommands;
}

mod general_command;
mod registered_command;
mod unregistered_command;

use teloxide::{
    dispatching::UpdateFilterExt,
    types::{BotCommand, Update},
};

use prelude::*;

pub(crate) use general_command::Command as GeneralCommand;
pub(crate) use unregistered_command::Command as UnregisteredCommand;

use super::{MainState, MyHandler};

pub fn build_command_handler() -> MyHandler {
    Update::filter_message()
        .enter_dialogue::<Message, StateStorage, MainState>()
        .branch(GeneralCommand::handler())
        .branch(
            dptree::entry()
                .map_user()
                .branch(UnregisteredCommand::handler())
                .branch(registered_command::build_registered_command_handler()),
        )
}

pub fn build_command() -> Vec<BotCommand> {
    let mut my_vec = Vec::new();

    my_vec.append(&mut registered_command::build_command());
    my_vec.append(&mut GeneralCommand::bot_commands());
    my_vec.append(&mut UnregisteredCommand::bot_commands());

    my_vec
}
