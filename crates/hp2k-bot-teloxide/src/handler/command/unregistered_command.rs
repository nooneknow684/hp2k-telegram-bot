use crate::handler::conversation::RegisterState;

use super::prelude::*;

#[derive(Debug, Clone, BotCommands)]
#[command(rename_rule = "lowercase")]
pub enum Command {
    #[command(rename = "kenalan", description = "[General] Kenalan dengan bot")]
    Register,
}

impl Command {
    pub fn handler() -> MyHandler {
        dptree::entry().filter_command::<Command>().branch(
            dptree::case![Command::Register]
                .branch(
                    dptree::entry()
                        .otw_unregistered()
                        .endpoint(RegisterState::start_handler),
                )
                .endpoint(|bot: Bot, message: Message| async move {
                    bot.send_message(message.chat.id, "Halo udah kenalan ya dong")
                        .await?;
                    Ok(())
                }),
        )
    }
}
