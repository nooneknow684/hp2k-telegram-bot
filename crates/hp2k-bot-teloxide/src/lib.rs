use std::sync::Arc;

use di::{Injectable, ServiceCollection};
use options::ext::OptionsConfigurationServiceExtensions;

mod handler;
mod prelude {
    pub use teloxide::prelude::*;
}

mod bot;
mod dispatcher;
mod task;

use bot::TeloxideBotOption;
use dispatcher::TeloxideDispatcherOption;

pub use bot::TeloxideBot;
pub use dispatcher::TeloxideDispatcher;
pub use task::{DoctorAskOtwTaskRunner, DoctorAskScheduleTaskRunner};

pub trait WithTeloxideBot {
    fn with_teloxide_bot(&mut self, config: &Arc<dyn config::ConfigurationRoot>) -> &mut Self;
}

impl WithTeloxideBot for ServiceCollection {
    fn with_teloxide_bot(&mut self, config: &Arc<dyn config::ConfigurationRoot>) -> &mut Self {
        self.apply_config::<TeloxideBotOption>(config.section("telegram").as_config().into());

        self.try_add(TeloxideBot::singleton())
    }
}
pub trait WithTeloxideDispatcher {
    fn with_teloxide_dispatcher(
        &mut self,
        config: &Arc<dyn config::ConfigurationRoot>,
    ) -> &mut Self;
}

impl WithTeloxideDispatcher for ServiceCollection {
    fn with_teloxide_dispatcher(
        &mut self,
        config: &Arc<dyn config::ConfigurationRoot>,
    ) -> &mut Self {
        self.with_teloxide_bot(config)
            .apply_config::<TeloxideDispatcherOption>(
                config.section("telegram").as_config().into(),
            );
        self.try_add(TeloxideDispatcher::singleton())
    }
}
