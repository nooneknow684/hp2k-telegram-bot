use crate::{
    handler::{self, MainState},
    prelude::*,
    TeloxideBot,
};
use std::sync::Arc;

use config::{
    ext::{ChainedBuilderExtensions, MemoryConfigurationBuilderExtensions},
    ConfigurationBuilder, ConfigurationRoot, DefaultConfigurationBuilder,
};
use di::injectable;
use hp2k_bot_core::{BaseTaskDispatcher, StorageService};
use options::Options;
use teloxide::{
    dispatching::{dialogue::InMemStorage, DefaultKey, Dispatcher},
    update_listeners::webhooks,
    Bot,
};

use crate::handler::build_handler;
use tracing::info;

#[derive(Debug, Default, serde::Deserialize)]
pub struct TeloxideDispatcherOption {
    pub ip: String,
    pub port: String,
    pub addr: String,
}

pub struct TeloxideDispatcher {
    option: Arc<TeloxideDispatcherOption>,
    bot: Arc<TeloxideBot>,
    db: Arc<dyn StorageService>,
    task_dispatcher: Arc<BaseTaskDispatcher>,
}

#[injectable]
impl TeloxideDispatcher {
    pub fn new(
        option: Arc<dyn Options<TeloxideDispatcherOption>>,
        bot: Arc<TeloxideBot>,
        db: Arc<dyn StorageService>,
        task_dispatcher: Arc<BaseTaskDispatcher>,
    ) -> Self {
        info!("Initializing teloxide dispatcher");
        Self {
            bot,
            db,
            task_dispatcher,
            option: option.value(),
        }
    }
    pub async fn create_dispatcher(&self) -> Dispatcher<Bot, anyhow::Error, DefaultKey> {
        let handler = build_handler();

        self.bot
            .set_my_commands(handler::build_command())
            .await
            .expect("error when setting command");

        Dispatcher::builder(self.bot.0.clone(), handler)
            .dependencies(dptree::deps![
                self.db.clone(),
                self.task_dispatcher.clone(),
                InMemStorage::<MainState>::new()
            ])
            .enable_ctrlc_handler()
            .build()
    }
    pub async fn start_polling(&self) {
        let mut dispatcher = self.create_dispatcher().await;
        dispatcher.dispatch().await
    }
    pub async fn start_webhook(&self) {
        let mut dispatcher = self.create_dispatcher().await;

        let socket = format!("{}:{}", self.option.ip, self.option.port)
            .parse()
            .expect("error when parsing socket");
        let url = self
            .option
            .addr
            .parse()
            .expect("error when parsing webhooks endpoint");

        let webhook_listener =
            webhooks::axum(self.bot.0.clone(), webhooks::Options::new(socket, url))
                .await
                .expect("error when creating webhook listener");

        dispatcher
            .dispatch_with_listener(
                webhook_listener,
                LoggingErrorHandler::with_custom_text("error when handling update"),
            )
            .await;
    }

    pub fn default_config() -> Box<dyn ConfigurationRoot> {
        DefaultConfigurationBuilder::new()
            .add_configuration(TeloxideBot::default_config().as_config())
            .add_in_memory(&[
                ("telegram:ip", "127.0.0.1"),
                ("telegram:port", "8080"),
                (
                    "telegram:addr",
                    "https://e36f-2404-c0-5a20-00-3ba-ea13.ngrok-free.app",
                ),
            ])
            .build()
            .unwrap()
    }
}
